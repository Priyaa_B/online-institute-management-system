<?php 
session_start();
require_once("database/conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();
$records = $con->query("SELECT * FROM courses");
$courses = array();
if($records){
   while ($obj = $records->fetch_assoc()) {
      $courses[] = $obj;
   }
}
?>

<?php include('includes/head.php') ?>
	  <section id="title" style="background-image: url(assets/images/home-hero.jpg)">
      <div class="container">
         <h1>Our Courses</h1>
      </div>  
     </section>

     <section class="in-sect">
        <div class="container">
          
           <div class="row align-left">
           <?php 
             foreach($courses as $key => $course){
               
           ?>
              <div class="col-md-4">
                 <div class="c-box">
                    <h4><?php 
                    $name = explode(" ", $course['course_name'], 3);
                     echo $name[0]." ".$name[1] ?></h4>
                    <h2><?php echo $name[2] ?></h2>
                    <p><?php echo substr($course['description'], 0, 100). " ... " ; ?></p>
                    <a href="view-detail.php?Id=<?php echo $course['course_id']?>" class="btn btn-info">View Details</a>
                 </div>
              </div>
          
           <?php } ?>
           </div>
        </div>
     </section>
	  

	  
     <?php include('includes/foot.php') ?>
   </body>
</html>