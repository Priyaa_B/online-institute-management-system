<?php
session_start();
require_once("database/conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

if(isset($_SESSION['user'])){

		header('Location:home.php');
		exit;
	
}
// login


if(isset($_POST['login'])){
	$_SESSION['msg_time'] =  time();
	$user_check = $con->query("SELECT user_id,role_id,name FROM users WHERE username='".$_POST['username']."' AND password='".Encrypt($_POST['password'])."'");
	$user = array();
	if($user_check){
		while($object = $user_check->fetch_assoc()){
			$user[] = $object;
		}
	}
	if(count($user) > 0){
		$_SESSION['user'] = $user[0];
		
		if($user[0]['role_id'] == ADMIN_ROLE_ID){
			header('Location:'.ADMIN_PATH.'students/index.php');
			exit;
		}elseif($user[0]['role_id'] == STAFF_ROLE_ID){
			header('Location:'.ADMIN_PATH.'classes/index.php');
			exit;
		}elseif($user[0]['role_id'] == STUDENT_ROLE_ID){
			header('Location:'.ADMIN_PATH.'student_panel/exams/index.php');
			exit;
		}
		header('Location:courses.php');
		exit;
	}else{
		$_SESSION['message_type'] = 'danger';
		$_SESSION['message'] = 'Username or password is invalid. Please check';
		header('Location:login.php');
		exit;
	}
}
?>
<?php include('includes/head.php')?>
      <section class="hero-wrap" style="background-image: url(assets/images/crop-woman.jpg)">
         <div class="container">
            <h1>Login</h1>
         </div>
      </section>
      
	  <section class="login-wrap">
		<div class="container">
			<?php include('includes/alert.php') ?>
			<div class="col-md-12 col-sm-12">
				<div class="login-form">
					<h2>Login</h2>
					<form method="post">
						 <div class="form-group col-md-12">
							<label for="username">Username</label>
							<input type="text" class="form-control" name="username" id="username" placeholder="Enter Username" required>
						</div>
						 <div class="form-group col-md-12">
							<label for="Name">Password</label>
							<input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" required>
						</div>
						<div class="clearfix"></div>
						<div class="col-sm-12"><button type="submit" name="login" class="btn">Submit</button></div>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
			
		</div>
	  </section>
	  
	 <?php include("includes/foot.php") ?>
   </body>
</html>