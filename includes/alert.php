<?php
if (isset($_SESSION['msg_time']) && (time() - $_SESSION['msg_time']) > 5 || isset($_SESSION['s_time']) && (time() - $_SESSION['s_time']) > 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time']) > 5) {
   unset($_SESSION['msg_time'], $_SESSION['message_type'], $_SESSION['message']);
   unset($_SESSION['u_time']);
}
?>

<div class="alert-message-block">
</div>
<?php 
$error_message_type = "";
$error_message = "";
if (isset($_SESSION['message_type']) && isset($_SESSION['message'])) { 
    $error_message_type = $_SESSION['message_type'];
    $error_message = $_SESSION['message'];
    echo "<script>sessionStorage.setItem('message_type','".$error_message_type."');</script>";
    echo "<script>sessionStorage.setItem('message','".$error_message."');</script>";
    
}
?>