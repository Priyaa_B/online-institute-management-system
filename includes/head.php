<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Home</title>
      <!-- Bootstrap -->
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600&display=swap" rel="stylesheet">
      <link href="<?php echo PROJECT_BASE_PATH ?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="assets/css/frontend.css">
   </head>
   <body>
      <header>
         <nav class="navbar navbar-default">
            <div class="container">
               <!-- Brand and toggle get grouped for better mobile display -->
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="home.php"><img src="assets/images/Victorian-logo.png" alt=""/></a>
               </div>
               <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="collapse navbar-collapse" id="navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                     <li><a href="home.php">Home</a></li>
                     <li><a href="about.php">About</a></li>
                     <li><a href="courses.php">Courses</a></li>
                     <?php if(isset($_SESSION['user'])){ ?>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['user']['name'] ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                           <li><a href="logout.php">Logout</a></li>
                        </ul>
                     </li>
                        <?php }else{ ?>
                         <li><a href="login.php">Login</a></li>
                     <?php } ?> 
                  </ul>
               </div>
               <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
         </nav>
         <!-- /.navbar -->
      </header>