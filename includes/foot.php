<footer>
         <p>Copyright © 2019 - 2021 Victorian Post Secondary Institute</p>
      </footer>
      <script src="<?php echo PROJECT_BASE_PATH ?>/assets/js/jquery.min.js"></script>
      <script src="<?php echo PROJECT_BASE_PATH ?>/assets/js/bootstrap.js"></script>
      <script>
    $(function () {
        var message_type = sessionStorage.getItem("message_type");
        var message = sessionStorage.getItem("message");
        if (message_type && message) {
            var alert = '<div class="alert alert-'+message_type+' alert-dismissible show" role="alert">'+message+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            $('.alert-message-block').html(alert);
            sessionStorage.removeItem("message_type");
            sessionStorage.removeItem("message");
        }
    });
</script>