<?php

function redirect_to($location = NULL) {
    if($location != NULL){
        header("Location: {$location}");
        exit;
    }
}

function Encrypt($password){
     $encryptedString = base64_encode($password);
     return $encryptedString; 
}

function Decrypt($password){
     $encryptedString = base64_decode($password);
     return $encryptedString; 
}

function ReplaceSql($str) {
    $str = trim(stripslashes($str));
    $str = str_replace("\\", "", $str);
    $str = str_replace("'", "\'", $str);
    return $str;
  }