<?php
//----------------------------------Role Constants------------------------
//Admin User Role
define('ADMIN_ROLE_ID','1');
//Staff User Role
define('STAFF_ROLE_ID','2');
//Students User Role
define('STUDENT_ROLE_ID','3');
//----------------------------------Path Constants------------------------
//Base PAth
define("PROJECT_BASE_PATH", "/online-institute-management-system");
//Complete Directory Path
define("DIRECTORY_PATH", $_SERVER['DOCUMENT_ROOT'] . PROJECT_BASE_PATH);
//
define("ADMIN_PATH",PROJECT_BASE_PATH."/admin/");
//
define("UPLOAD_PATH_ORG", DIRECTORY_PATH . "/uploads/");
//
define("FRONTEND_UPLOAD_PATH_ORG", PROJECT_BASE_PATH . "/uploads/");
//----------------------------------DB Constants--------------------------
//DB - User Name
define("DB_USER", "root");
//DB - Password
define("DB_PASS", "");
//DB - Name
define("DB_NAME", "assignment_online_institute_system");

//----------------------------------Document Type Constants--------------
//Assessments
define("ASSESSMENT_QUESTION",1);
//
define("ASSESSMENT_ANSWER",2);
//Exams
define("EXAM_QUESTION",3);
//
define("EXAM_ANSWER",4);
//--------------------------------Document Path--------------------------
//Assessment
define('ASSESSMENT_QUES_FILE_PATH',DIRECTORY_PATH.'/admin/documents/assessment_questions/');
define('ASSESSMENT_QUES_FILE_DOWNLOAD_PATH',ADMIN_PATH.'documents/assessment_questions/');
define('ASSESSMENT_ANS_FILE_PATH',DIRECTORY_PATH.'/admin/documents/assessment_answers/');
define('ASSESSMENT_ANS_FILE_DOWNLOAD_PATH',ADMIN_PATH.'/documents/assessment_answers/');
//Exams
define('EXAM_QUES_FILE_PATH',DIRECTORY_PATH.'/admin/documents/exam_questions/');
define('EXAM_QUES_FILE_DOWNLOAD_PATH',ADMIN_PATH.'documents/exam_questions/');
define('EXAM_ANS_FILE_PATH',DIRECTORY_PATH.'/admin/documents/exam_answers/');
define('EXAM_ANS_FILE_DOWNLOAD_PATH',ADMIN_PATH.'documents/exam_answers/');
?>