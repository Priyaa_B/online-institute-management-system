<?php

define("BASE_PATH", "/online-school-");
define("APPLICATION_PATH", $_SERVER['DOCUMENT_ROOT'] . BASE_PATH);
define("UPLOAD_PATH_ORG", APPLICATION_PATH . "/uploads/");
define("FRONTEND_UPLOAD_PATH_ORG", BASE_PATH . "/uploads/");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "assignment_online_equity");
define('STOCK_SEARCH_API_KEY','c1a4qa748v6te7ig4rc0');
define('STOCK_LIST_API_KEY','f7a670b52ff8b8ae15e5c1dbb40a6855');
define('FCS_API_KEY','uUdHiZIvX0AjT4JOPML9gt3N8');
define('CUSTOMER_ROLE_ID','2');

//stock buy/sell action
define('ACTION_BUY','1');
define('ACTION_SELL','2');
define('BUY','buy');
define('SELL','sell');

//Wallet History
define('TYPE_WITHDRAWN','1');
define('TYPE_DEPOSITED','2');
define('WITHDRAWN','withdrawn');
define('DEPOSITED','deposited');
$history_type = array('1' =>'Withdrawn', '2' => 'Deposited');

//Wallet action text
define('WITHDRAWN_TEXT','Bought Share -');
define('DEPOSIT_TEXT','Amount added to wallet');
define('STOCK_SELL_DEPOSITED_TEXT','Share sold -');