   <!--header-->
   <header>
         <div class="col-md-6 col-sm-8 col-xs-8">
            <div class="col-md-3 col-sm-3 col-xs-4">
               <div class="logo">
                  <h3>Logo</h3>
               </div>
            </div>
            <div class="col-md-6 col-sm-9 col-xs-8">
               <div class="search-bar">
                  <input type="search" value="" placeholder="Search">
                  <div class="submit-btn">
                     <button><i class="fa fa-search" aria-hidden="true"></i></button>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-6 col-sm-4 col-xs-4">
			<div class="user">
				<div class="userimg">
					<img src="images/user.jpg">
				</div>
				<div class="username">
					<h3>Patrick <span><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3>
				</div>
				
				<div class="user-menu" style="display: none;">
					<ul>
						<li><a href="#">Edit Profile</a></li>
						<li><a href="#">Logout</a></li>
					</ul>
				</div>
				
			</div>
         </div>
      </header>
	  <!--header-->