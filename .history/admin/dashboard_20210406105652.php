<?php require_once(dirname(__DIR__).'/classes/SqlQueries.php') ?>
<?php include(dirname(__DIR__).'/admin/includes/head.php') ?>
<?php include(dirname(__DIR__).'/admin/includes/header.php') ?>
<?php include(dirname(__DIR__).'/admin/includes/sidebar.php') ?>


      <section class="page-content">
         <div class="form-heading">
            <h3>Dashboard</h3>
         </div>
		 
		 <!--dashboard-open-->
         <div class="dashboard">
            <div class="top-cards">
               <div class="card">
                  <div class="bodycard">
                     <div class="icon">
                        <i class="fa fa-home" aria-hidden="true"></i>
                     </div>
                     <div class="title">
                        <h3>New Projects</h3>
                     </div>
                     <div class="numbring">
                        <h4>50</h4>
                     </div>
                  </div>
               </div>
               <div class="card">
                  <div class="bodycard">
                     <div class="icon">
                        <i class="fa fa-home" aria-hidden="true"></i>
                     </div>
                     <div class="title">
                        <h3>New Projects</h3>
                     </div>
                     <div class="numbring">
                        <h4>50</h4>
                     </div>
                  </div>
               </div>
               <div class="card">
                  <div class="bodycard">
                     <div class="icon">
                        <i class="fa fa-home" aria-hidden="true"></i>
                     </div>
                     <div class="title">
                        <h3>New Projects</h3>
                     </div>
                     <div class="numbring">
                        <h4>50</h4>
                     </div>
                  </div>
               </div>
               <div class="card">
                  <div class="bodycard">
                     <div class="icon">
                        <i class="fa fa-home" aria-hidden="true"></i>
                     </div>
                     <div class="title">
                        <h3>New Projects</h3>
                     </div>
                     <div class="numbring">
                        <h4>50</h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
		 <!--dashboard-closed-->
		 
		 <!--section-open-->
         <section class="table-listing">
            <div class="row">
               <div class="col-sm-12">
                  <div class="card">
                     <div class="table-responsive">
                        <table class="table">
                           <thead>
                              <tr>
                                 <th class="border-top-0">Products</th>
                                 <th class="border-top-0">License</th>
                                 <th class="border-top-0">Support Agent</th>
                                 <th class="border-top-0">Technology</th>
                                 <th class="border-top-0">Tickets</th>
                                 <th class="border-top-0">Sales</th>
                                 <th class="border-top-0">Earnings</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </section>
		 <!--section-closed-->
		 
		 <!--lower-card-->
		 <section class="lower-card">
			<div class="row">
				<div class="col-md-6">
					<div class="card-title">
						<h3>Recent post</h3>
					</div>
					<div class="main-card">
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="card-title">
						<h3>Recent post</h3>
					</div>
					<div class="main-card">
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
					</div>
				</div>
			</div>
		 </section>
		 <!--lower-card-->
		 
      </section>
      <?php include(dirname(__DIR__).'/admin/includes/sideb.php') ?>
   </body>
</html>

