<?php
require_once(dirname(__DIR__, 2) . '/classes/SqlQueries.php');
$query = new SqlQueries();

if (isset($_POST['add_staff'])) {
   $_SESSION['msg_time'] = time();
   $user_id = $query->InsertQuery("INSERT INTO users SET role_id=".STAFF_ROLE_ID.", name='".$_POST['first_name']."', username='".$_POST['username']."',password='".$_POST['password']."' "); 
   $staff_id = $query->InsertQuery("INSERT INTO staffs SET user_id=".$user_id.", first_name='".$_POST['first_name']."', last_name='".$_POST['last_name']."', contact_no='".$_POST['contact_no']."',age='".$_POST['age']."',gender='".$_POST['gender']."',email='".$_POST['email']."',address='".$_POST['address']."' "); 
   if (!empty($result)) {
      $_SESSION['level'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
  } else {
      $_SESSION['level'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
      $message = "Record cannot be added";
  }
  header('Location:index.php');
}
?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/head.php') ?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/header.php') ?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/sidebar.php') ?>


<!--page-content-->
<section class="page-content">
<?php include('../includes/alert.php'); ?>
   <div class="form-heading">
   <div class="row">
    <div class="col-md-6">
      <h3>STAFF LISTING</h3>
    </div>
    <div class="col-md-6 text-right">
      <a class="btn btn-warning" href="form.php"><i class="fa fa-plus-circle mr-1" aria-hidden="true"></i> &nbsp; Add Staff</a>
    </div>
    </div>
   </div>
   <form method="post">
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
         </div>
         <div class="form-group col-md-6">
            <label for="last_name">Last Name</label>
            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name">
         </div>

      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="email">
         </div>
         <div class="form-group col-md-6">
            <label for="contact_no">Contact No</label>
            <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Contact No">
         </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="age">Age</label>
            <input type="number" class="form-control" name="age" id="age" placeholder="Age">
         </div>
         <div class="form-group col-md-6">
            <label for="gender">Gender</label>
            <input type="text" class="form-control" name="gender" id="gender" placeholder="Gender">
         </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Username">
         </div>
         <div class="form-group col-md-6">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
         </div>
      </div>
      <div class="form-group">
         <label for="sddress">Address</label>
         <textarea class="form-control" id="address" name="address" rows="3"></textarea>
      </div>
      <!-- <div class="form-group">
         <label for="uploader">File upload</label>
         <input type="file" class="form-control" name="image" id="uploader">
      </div> -->
      <div class="form-btn">
         <button type="submit" name="add_staff" class="btn btn-primary">Submit</button>
         <button type="reset" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(dirname(__DIR__, 2) . '/admin/includes/foot.php') ?>
</body>

</html>