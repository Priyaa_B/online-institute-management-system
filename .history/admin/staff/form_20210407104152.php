<?php
require_once(dirname(__DIR__, 2) . '/classes/SqlQueries.php');
$query = new SqlQueries();

if (isset($_POST['add_staff'])) {

   $user_id = $query->InsertQuery("INSERT INTO users SET role_id=".STAFF_ROLE_ID.", name='".$_POST['first_name']."', username='".$_POST['username']."',password='".$_POST['password']."' "); 
   $user_id = $query->InsertQuery("INSERT INTO staffs SET user_id=".$user_id.", first_name='".$_POST['first_name']."', last_name='".$_POST['last_name']."', username='".$_POST['username']."',password='".$_POST['password']."' "); 

}
?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/head.php') ?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/header.php') ?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/sidebar.php') ?>


<!--page-content-->
<section class="page-content">
   <div class="form-heading">
      <h3>ADD STAFF</h3>
   </div>
   <form method="post">
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
         </div>
         <div class="form-group col-md-6">
            <label for="last_name">Last Name</label>
            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name">
         </div>

      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="email">
         </div>
         <div class="form-group col-md-6">
            <label for="contact_no">Contact No</label>
            <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Contact No">
         </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="age">Age</label>
            <input type="number" class="form-control" name="age" id="age" placeholder="Age">
         </div>
         <div class="form-group col-md-6">
            <label for="gender">Gender</label>
            <input type="text" class="form-control" name="gender" id="gender" placeholder="Gender">
         </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Username">
         </div>
         <div class="form-group col-md-6">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
         </div>
      </div>
      <div class="form-group">
         <label for="sddress">Address</label>
         <textarea class="form-control" id="address" name="address" rows="3"></textarea>
      </div>
      <!-- <div class="form-group">
         <label for="uploader">File upload</label>
         <input type="file" class="form-control" name="image" id="uploader">
      </div> -->
      <div class="form-btn">
         <button type="submit" name="add_staff" class="btn btn-primary">Submit</button>
         <button type="reset" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(dirname(__DIR__, 2) . '/admin/includes/foot.php') ?>
</body>

</html>