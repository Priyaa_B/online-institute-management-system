<?php
require_once(dirname(__DIR__, 2) . '/classes/SqlQueries.php');
$query = new SqlQueries();

?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/head.php') ?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/header.php') ?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/sidebar.php') ?>


<!--page-content-->
<section class="page-content">
   <div class="form-heading">
      <h3>Add Staff</h3>
   </div>
   <form>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="email">Fi</label>
            <input type="email" class="form-control" id="email" placeholder="Email">
         </div>
         <div class="form-group col-md-6">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" placeholder="Password">
         </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="email">Time</label>
            <input type="time" class="form-control" id="time" placeholder="time">
         </div>
         <div class="form-group col-md-6">
            <label for="Date">Date</label>
            <input type="date" class="form-control" id="Date" placeholder="Date">
         </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" placeholder="Email">
         </div>
         <div class="form-group col-md-6">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" placeholder="Password">
         </div>
      </div>
      <div class="form-group">
         <label for="Address">Address</label>
         <input type="text" class="form-control" id="Address" placeholder="1234 Main St">
      </div>
      <div class="form-group">
         <label for="Address2">Address 2</label>
         <input type="text" class="form-control" id="Address2" placeholder="Apartment, studio, or floor">
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="city">City</label>
            <input type="text" class="form-control" id="city">
         </div>
         <div class="form-group col-md-4">
            <label for="inputState" class="col-form-label">State</label>
            <select id="inputState" class="form-control">
               <option>Choose</option>
               <option>Option 1</option>
               <option>Option 2</option>
               <option>Option 3</option>
            </select>
         </div>
         <div class="form-group col-md-2">
            <label for="zip" class="col-form-label">Zip</label>
            <input type="text" class="form-control" id="zip">
         </div>
      </div>
      <div class="form-group">
         <label for="uploader">File upload</label>
         <input type="file" class="form-control" id="uploader">
      </div>
      <div class="form-btn">
         <button type="submit" class="btn btn-primary">Submit</button>
         <button type="submit" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(dirname(__DIR__, 2) . '/admin/includes/foot.php') ?>
</body>

</html>