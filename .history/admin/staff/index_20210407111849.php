<?php
require_once(dirname(__DIR__, 2) . '/classes/SqlQueries.php');
$query = new SqlQueries();

if (isset($_POST['add_staff'])) {
   $_SESSION['msg_time'] = time();
   $user_id = $query->InsertQuery("INSERT INTO users SET role_id=".STAFF_ROLE_ID.", name='".$_POST['first_name']."', username='".$_POST['username']."',password='".$_POST['password']."' "); 
   $staff_id = $query->InsertQuery("INSERT INTO staffs SET user_id=".$user_id.", first_name='".$_POST['first_name']."', last_name='".$_POST['last_name']."', contact_no='".$_POST['contact_no']."',age='".$_POST['age']."',gender='".$_POST['gender']."',email='".$_POST['email']."',address='".$_POST['address']."' "); 
   if (!empty($result)) {
      $_SESSION['level'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
  } else {
      $_SESSION['level'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
      $message = "Record cannot be added";
  }
  header('Location:index.php');
}
?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/head.php') ?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/header.php') ?>
<?php include(dirname(__DIR__, 2) . '/admin/includes/sidebar.php') ?>


<!--page-content-->
<section class="page-content">
<?php include('../includes/alert.php'); ?>
<div class="form-heading">
    <div class="row">
    <div class="col-md-6">
      <h3>STAFF LISTING</h3>
    </div>
    <div class="col-md-6 text-right">
      <a class="btn btn-warning"><i clssAdd Staff</a>
    </div>
    </div>
   </div>
 		 <!--section-open-->
          <section class="table-listing">
            <div class="row">
               <div class="col-sm-12">
                  <div class="card">
                     <div class="table-responsive">
                        <table class="table">
                           <thead>
                              <tr>
                                 <th class="border-top-0">S. No.</th>
                                 <th class="border-top-0">Name</th>
                                 <th class="border-top-0">Email</th>
                                 <th class="border-top-0">Contact No</th>
                                 <th class="border-top-0">Age</th>
                                 <th class="border-top-0">Earnings</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </section>
		 <!--section-closed-->
</section>
<!--page-content-->
<?php include(dirname(__DIR__, 2) . '/admin/includes/foot.php') ?>
</body>

</html>