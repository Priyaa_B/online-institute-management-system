<?php include(dirname(__DIR__).'/admin') ?>
   <!--header-->
      <header>
         <div class="col-md-6 col-sm-8 col-xs-8">
            <div class="col-md-3 col-sm-3 col-xs-4">
               <div class="logo">
                  <h3>Logo</h3>
               </div>
            </div>
            <div class="col-md-6 col-sm-9 col-xs-8">
               <div class="search-bar">
                  <input type="search" value="" placeholder="Search">
                  <div class="submit-btn">
                     <button><i class="fa fa-search" aria-hidden="true"></i></button>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-6 col-sm-4 col-xs-4">
			<div class="user">
				<div class="userimg">
					<img src="images/user.jpg">
				</div>
				<div class="username">
					<h3>Patrick <span><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3>
				</div>
				
				<div class="user-menu" style="display: none;">
					<ul>
						<li><a href="#">Edit Profile</a></li>
						<li><a href="#">Logout</a></li>
					</ul>
				</div>
				
			</div>
         </div>
      </header>
	  <!--header-->
	  
	  <!--left-side-menu-->
      <div class="left-side-menu">
         <ul class="menu-wrap">
            <li class="label-title">Navigation</li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li>
               <a href="#">
                  <i class="fa fa-home" aria-hidden="true"></i><span>Home</span>
                  <div class="arrow-right"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
               </a>
               <ul class="sub-menu">
                  <li><a href="#">Typography</a></li>
                  <li><a href="#">Typography</a></li>
                  <li><a href="#">Typography</a></li>
                  <li><a href="#">Typography</a></li>
               </ul>
            </li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
         </ul>
      </div>
	  <!--left-side-menu-->
	  
	  
      <section class="page-content">
         <div class="form-heading">
            <h3>Dashboard</h3>
         </div>
		 
		 <!--dashboard-open-->
         <div class="dashboard">
            <div class="top-cards">
               <div class="card">
                  <div class="bodycard">
                     <div class="icon">
                        <i class="fa fa-home" aria-hidden="true"></i>
                     </div>
                     <div class="title">
                        <h3>New Projects</h3>
                     </div>
                     <div class="numbring">
                        <h4>50</h4>
                     </div>
                  </div>
               </div>
               <div class="card">
                  <div class="bodycard">
                     <div class="icon">
                        <i class="fa fa-home" aria-hidden="true"></i>
                     </div>
                     <div class="title">
                        <h3>New Projects</h3>
                     </div>
                     <div class="numbring">
                        <h4>50</h4>
                     </div>
                  </div>
               </div>
               <div class="card">
                  <div class="bodycard">
                     <div class="icon">
                        <i class="fa fa-home" aria-hidden="true"></i>
                     </div>
                     <div class="title">
                        <h3>New Projects</h3>
                     </div>
                     <div class="numbring">
                        <h4>50</h4>
                     </div>
                  </div>
               </div>
               <div class="card">
                  <div class="bodycard">
                     <div class="icon">
                        <i class="fa fa-home" aria-hidden="true"></i>
                     </div>
                     <div class="title">
                        <h3>New Projects</h3>
                     </div>
                     <div class="numbring">
                        <h4>50</h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
		 <!--dashboard-closed-->
		 
		 <!--section-open-->
         <section class="table-listing">
            <div class="row">
               <div class="col-sm-12">
                  <div class="card">
                     <div class="table-responsive">
                        <table class="table">
                           <thead>
                              <tr>
                                 <th class="border-top-0">Products</th>
                                 <th class="border-top-0">License</th>
                                 <th class="border-top-0">Support Agent</th>
                                 <th class="border-top-0">Technology</th>
                                 <th class="border-top-0">Tickets</th>
                                 <th class="border-top-0">Sales</th>
                                 <th class="border-top-0">Earnings</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
							  
							  <tr>
                                 <td>
                                    <div class="display-flex">
                                       <h4>Elite Admin</h4>
                                    </div>
                                 </td>
                                 <td>Single Use</td>
                                 <td>John Doe</td>
                                 <td>
                                    <label class="label label-danger">Angular</label>
                                 </td>
                                 <td>46</td>
                                 <td>356</td>
                                 <td>
                                    <h5>$2850.06</h5>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </section>
		 <!--section-closed-->
		 
		 <!--lower-card-->
		 <section class="lower-card">
			<div class="row">
				<div class="col-md-6">
					<div class="card-title">
						<h3>Recent post</h3>
					</div>
					<div class="main-card">
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="card-title">
						<h3>Recent post</h3>
					</div>
					<div class="main-card">
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
						
						<div class="cardbox">
							<div class="card-img">
							<img src="images/user.jpg" alt="user">
						</div>
						<div class="card-info">
							<h3>James Anderson</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
						</div>
						</div>
					</div>
				</div>
			</div>
		 </section>
		 <!--lower-card-->
		 
      </section>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script>
         jQuery(document).ready(function(){
         	jQuery('.left-side-menu ul li a').click(function(){
         		jQuery('.sub-menu').slideToggle('slow');
         	})
         
         jQuery('.user .username h3 span').click(function(){
         jQuery('.user-menu').slideToggle('slow');
         })
         })
          
      </script>
   </body>
</html>

