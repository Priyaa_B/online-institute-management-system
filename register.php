<?php
session_start();
require_once("database/conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] != ADMIN_ROLE_ID){
		header('Location:home.php');
		exit;	
}

if(isset($_POST['register'])){
	$_SESSION['msg_time'] =  time();
	$username= $con->real_escape_string(htmlspecialchars( $_POST['username'])); 
	if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
        $check_mail = $con->query("SELECT * FROM students WHERE email='".$_POST['email']."'");
		$mail = array();
		if($check_mail){
			while($object = $check_mail->fetch_assoc() ){
				$mail[] =$object; 
			}
		}

		if(count($mail) == 1){
			$user_check = $con->query("SELECT * FROM users INNER JOIN students ON students.user_id = users.user_id WHERE students.email='".$mail[0]['email']."'");
			$user = array();
			if($user_check){
				while($obj = $user_check->fetch_assoc() ){
					$user[] =$obj; 
				}
			}
			if(count($user)>0){
				$_SESSION['message_type'] = 'danger';
				$_SESSION['message'] = 'User with this email already exists.';
				header('Location:login.php');
     			 }else{
					if($con->query("INSERT INTO users SET name='".$mail[0]['first_name']."' ,role_id=".STUDENT_ROLE_ID.", username='".$_POST['username']."',password='".Encrypt($_POST['password'])."'")){
						$user_id = $con->insert_id;
					}  
					$con->query("UPDATE students SET user_id=".$user_id." WHERE email='".$mail[0]['email']."'");
				      
					$_SESSION['message_type'] = 'success';
				$_SESSION['message'] = 'Registeration successfully done';
				header('Location:login.php');
				} 
     
			}else{

			}
		}
	
}

?>
<?php include('includes/head.php')?>
      <section class="hero-wrap" style="background-image: url(assets/images/crop-woman.jpg)">
         <div class="container">
            <h1>Login/Register</h1>
         </div>
      </section>
      
	  <section class="login-wrap">
		<div class="container">
			<?php include('includes/alert.php') ?>
			
			<div class="col-md-12 col-sm-12">
				<div class="login-form">
					<h2>Register</h2>
					<form method="post">
						 <div class="form-group col-md-12">
							<label for="Name">Email</label>
							<input type="Email" class="form-control" name="email" id="email" placeholder="Enter Your Email" required>
						</div>
						<div class="form-group col-md-6">
							<label for="Name">Username</label>
							<input type="text" class="form-control" name="username" id="username" placeholder="Create Your Username" required>
						</div>
						<div class="form-group col-md-6">
							<label for="Name">Password</label>
							<input type="password" class="form-control" name="password" id="password" placeholder="Create Your Password" required>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-12"><button type="submit" name="register" class="btn">Submit</button></div>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
		</div>
	  </section>
	  
	 <?php include("includes/foot.php") ?>
   </body>
</html>