<?php
session_start();
require_once("database/conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

if (isset($_GET['Id']) && !empty($_GET['Id'])) {
   $course_id = $_GET['Id'];
}
if (isset($_POST['course_apply'])) {
   $course_id = $_POST['course_id'];
}

if(isset($_POST['enroll_student'])){
   $_SESSION['msg_time'] =  time();   
   if($con->query("INSERT INTO students SET first_name = '".$_POST['first_name']."', last_name='".$_POST['last_name']."' , father_name='".$_POST['father_name']."', dob='".date('Y-m-d H:i:s',strtotime($_POST['dob']))."', email='".$_POST['email']."', contact_no='".$_POST['contact_no']."', address='".$_POST['address'].",".$_POST['city'].",".$_POST['state'].",".$_POST['zip']."', course_id='".$_POST['course_id']."', session_id='".$_POST['session_id']."'")){
     
         $_SESSION['message_type'] = 'success';
         $_SESSION['message'] = 'Enrollment Successfully done.Please register';
         header('Location:register.php');
      } else {
         $_SESSION['message_type'] = 'danger';
         $_SESSION['message'] = 'Some Error occured. Please try again';
         header('Location:enroll-now.php');
      }
      
   
}

$records = $con->query("SELECT * FROM courses");
$courses = array();
if($records){
   while ($obj = $records->fetch_assoc()) {
      $courses[] = $obj;
   }
}
$session_records = $con->query("SELECT * FROM sessions");
$sessions = array();
if($session_records){
   while ($obj = $session_records->fetch_assoc()) {
      $sessions[] = $obj;
   }
}
?>
<?php include('includes/head.php') ?>
<section class="hero-wrap" style="background-image: url(assets/images/crop-woman.jpg)">
   <div class="container">
      <h1>Enroll Now</h1>
   </div>
</section>
<section class="frontend-form">
   <?php include('includes/alert.php') ?>
   <div class="container">
      <div class="frontfrom">
         <form method="post">
            <div class="form-title col-md-12">
               <h2>Personal Details</h2>
            </div>
            <div class="form-row">
               <div class="form-group col-md-4 col-sm-4">
                  <label for="Name">Given Name(s):</label>
                  <input type="text" class="form-control" name="first_name" id="name" placeholder="Enter First Name" required>
               </div>
               <div class="form-group col-md-4 col-sm-4">
                  <label for="Name">Family Name or Surname:</label>
                  <input type="text" class="form-control" name="last_name" id="name" placeholder="Enter Last Name" required>
               </div>
              
               <div class="form-group col-md-4 col-sm-4">
                  <label for="Name">Father Name:</label>
                  <input type="text" class="form-control" name="father_name" id="name" placeholder="Enter Last Name" required>
               </div>
              
            </div>
            <div class="form-row">
            <div class="form-group col-md-6 col-sm-6">
                  <label for="gender" class="col-form-label">Gender</label>
                  <select id="gender" name="gender" class="form-control" required>
                     <option>Male</option>
                     <option>Female</option>
                     <option>Unspecified</option>
                  </select>
               </div>
               <div class="form-group col-md-6 col-sm-6">
                  <label for="Date">Date of Birth</label>
                  <input type="date" name="dob" class="form-control" id="Date" placeholder="Date" required>
               </div>
              
            </div>

            <div class="clearfix"></div>
            <div class="col-md-12">
               <hr>
            </div>
            <div class="clearfix"></div>

            <div class="form-title col-md-12">
               <h2>Course Details</h2>
            </div>
            <div class="form-row">
               <div class="form-group col-md-6 col-sm-6">
                  <label for="Name">Courses</label>
                  <select id="course_id" name="course_id" class="form-control" required>
                     <option value="">Choose Course</option>
                     <?php foreach($courses as $course){ ?>
                     <option value="<?php echo $course['course_id'] ?>" <?php echo $course_id == $course['course_id'] ? 'selected' : '' ?>><?php echo $course['course_name']?></option>
                     <?php } ?>
                  </select>
               </div>

               <div class="form-group col-md-6 col-sm-6">
                  <label for="Name">Session</label>
                  <select id="session_id" name="session_id" class="form-control" required>
                  <option value="">Choose Session</option>
                     <?php foreach($sessions as $session){ ?>
                     <option value="<?php echo $session['session_id'] ?>"><?php echo $session['session_name']?></option>
                     <?php } ?>
                  </select>
               </div>
            </div>


            <div class="clearfix"></div>
            <div class="col-md-12">
               <hr>
            </div>
            <div class="clearfix"></div>

            <div class="form-title col-md-12">
               <h2>Contact Details</h2>
            </div>

            <div class="form-row">
               <div class="form-group col-md-4 col-sm-6">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" name="email" id="email" value="<?php echo isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : "" ?>" placeholder="Enter Your Email" required>
               </div>
               <div class="form-group col-md-4 col-sm-6">
                  <label for="Address">Mobile</label>
                  <input type="text" class="form-control" name="contact_no" value="<?php echo isset($_POST['contact_no']) && !empty($_POST['contact_no']) ? $_POST['contact_no'] : "" ?>" id="Address" placeholder="Enter Your Mobile" required>
               </div>
               <div class="form-group col-md-4 col-sm-12">
                  <label for="Address2">Address</label>
                  <input type="text" class="form-control" id="Address2" name="address" placeholder="Enter Apartment or floor" required>
               </div>
            </div>

            <div class="form-row">
               <div class="form-group col-md-4 col-sm-4">
                  <label for="city">City</label>
                  <input type="text" class="form-control" name="city" placeholder="Enter Your City" required>
               </div>
               <div class="form-group col-md-4 col-sm-4">
                  <label for="state" class="col-form-label">State</label>
                  <input type="text" class="form-control" name="city" placeholder="Enter Your State" required>
               </div>
               <div class="form-group col-md-4 col-sm-4">
                  <label for="zip" class="col-form-label">Zip</label>
                  <input type="text" class="form-control" name="zip" placeholder="Enter Your Zip Code" id="zip" required>
               </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-btn col-md-12">
               <button type="submit" name="enroll_student" class="btn btn-primary">Submit</button>
               <button type="reset" class="btn btn-danger">Cancel</button>
            </div>
            <div class="clearfix"></div>
         </form>
      </div>
   </div>
</section>


<?php include('includes/foot.php') ?>
</body>

</html>