<?php
session_start();
require_once("database/conn.php");
?>
<?php include('includes/head.php')?>
      <!--hero-home-->
      <section class="hero-home" style="background-image: url(assets/images/home-hero.jpg)">
         <div class="container">
         <div class="col-md-6">
         <div class="headings">
            <h1>Best University for Education</h1>
            <p>You might remember the Dell computer commercials in which a youth reports this exciting news to his friends.</p>
            <a href="#">Enroll Now</a>
            <div>
            </div>
         </div>
      </section>
      <!--hero-home-->
      <!--about-us-->
      <section class="about-us">
         <div class="container">
            <div class="row boxes-wrap">
               <div class="col-sm-6 col-md-3 col-lg-3">
                  <div class="about-box">
                     <div class="img">
                        <img src="assets/images/icon-1.webp" alt="">
                     </div>
                     <div class="info">
                        <h3>University</h3>
                        <h4>Overall in here</h4>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-3 col-lg-3">
                  <div class="about-box">
                     <div class="img">
                        <img src="assets/images/icon-1.webp" alt="">
                     </div>
                     <div class="info">
                        <h3>University</h3>
                        <h4>Overall in here</h4>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-3 col-lg-3">
                  <div class="about-box">
                     <div class="img">
                        <img src="assets/images/icon-1.webp" alt="">
                     </div>
                     <div class="info">
                        <h3>University</h3>
                        <h4>Overall in here</h4>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-3 col-lg-3">
                  <div class="about-box">
                     <div class="img">
                        <img src="assets/images/icon-1.webp" alt="">
                     </div>
                     <div class="info">
                        <h3>University</h3>
                        <h4>Overall in here</h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--about-us-->
	  
	  <!--about-collage-->
	  <section class="about-collage">
		<div class="container">
			<div class="col-md-6">
				<div class="col-left">
					<div class="collage-img">
						<img src="assets/images/home-hero.jpg" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="col-right">
					<h2>Welcome to The University</h2>
					<p>Nam in viverra mauris. Integer varius ullamcorper metus auctor porta. Pellentesque velit eros, malesuada sed lorem non, rhoncus blandit leo. Suspendisse lacus risus, sagittis vel laoreet at, placerat in ex. Sed placerat est sit amet est varius, mollis vulputate lorem varius. Praesent sollicitudin sem dignissim, venenatis ligula in, sodales augue. Nulla eleifend turpis at efficitur placerat. Proin id diam nisl. Aenean varius erat sit amet dui pulvinar.</p>
					
					<p>st sit amet est varius, mollis vulputate lorem varius. Praesent sollicitudin sem dignissim, venenatis ligula in, sodales augue. Nulla eleifend turpis at efficitur placerat. Proin id diam nisl. Aenean varius erat sit amet dui pulvinar.</p>
				</div>
			</div>
		</div>
	  </section>
	  <!--about-collage-->
	  
	  
	  <!--our-university-->
	  <section class="our-university" style="background-image: url(assets/images/our-university.jpg)">
		<div class="container">
			<div class="inner-box">
            <h2>Heading of the section goes here</h2>
            <p>Nam in viverra mauris. Integer varius ullamcorper metus auctor porta. Pellentesque velit eros, malesuada sed lorem non, rhoncus blandit leo. Suspendisse lacus risus, sagittis vel laoreet at, placerat in ex. Sed placerat est sit amet est varius, mollis vulputate lorem varius. Praesent sollicitudin sem dignissim, venenatis ligula in, sodales augue. Nulla eleifend turpis </p>      
         </div>
		</div>
	  </section>
	  <!--our-university-->


     <section id="courses-hm">
        <div class="container">
           <div class="row">
              <div class="col-md-4">
                 <ul>
                    <li><a href="#"><i class="fa fa-caret-right"></i> Bachelor Of Computer Science</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i> Bachelor of Agricultural Science</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i> Bachelor of Biomedical Science</a></li>
                 </ul>
              </div>
              <div class="col-md-4">
                 <ul>
                    <li><a href="#"><i class="fa fa-caret-right"></i> Master of Computer Science</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i> Master of Communication</a></li>
                 </ul>
              </div>
              <div class="col-md-4">
                 <ul>
                    <li><a href="#"><i class="fa fa-caret-right"></i> Bachelor of Communication</a></li>
                    <li><a href="#"><i class="fa fa-caret-right"></i> Master of Agricultural Science</a></li>
                 </ul>
              </div>
           </div>
        </div>
     </section>
	  
      <?php include('includes/foot.php') ?>
   </body>
</html>