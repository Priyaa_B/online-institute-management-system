<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Home</title>
      <!-- Bootstrap -->
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600&display=swap" rel="stylesheet">
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="assets/css/frontend.css">
   </head>
   <body>
      <header>
         <nav class="navbar navbar-default">
            <div class="container">
               <!-- Brand and toggle get grouped for better mobile display -->
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"><img src="assets/images/Victorian-logo.png" alt=""/></a>
               </div>
               <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="collapse navbar-collapse" id="navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                     <li><a href="home.php">Home</a></li>
                     <li><a href="about.php">About</a></li>
                     <li><a href="courses.php">Courses</a></li>
                     <li><a href="contact.php">Contact</a></li>
                  </ul>
               </div>
               <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
         </nav>
         <!-- /.navbar -->
      </header>


	  <section id="title" style="background-image: url(assets/images/home-hero.jpg)">
      <div class="container">
         <h1>Contact US</h1>
      </div>  
     </section>

     <section class="in-sect" id="contact">
        <div class="container">
          <div class="row">
            <div class="col-md-7">
              <div class="in-box">
                <h3 class="text-center">Have Some Questions?</h3>
                <p class="text-center">Fiil the form below if you have any query or questions?</p>

                <form>
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Email ID</label>
                    <input type="text" name="email" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Contact No</label>
                    <input type="text" name="contact" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Message</label>
                    <textarea class="form-control" name="message" style="height: 110px;"></textarea>
                  </div>
                  <div class="form-group">
                    <label></label>
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-md-5">
              <div class="img-box">
                <img src="assets/images/email.png">
              </div>
            </div>
          </div>
        </div>
     </section>
	  

	  
      <footer>
         <p>Copyright © 2019 - 2021 Victorian Post Secondary Institute</p>
      </footer>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="assets/js/bootstrap.js"></script>
   </body>
</html>