<?php
session_start();
require_once("database/conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

$course_id = $_GET['Id'];

$course_details = $con->query("SELECT * FROM courses WHERE course_id=" . $course_id . "");
$result = array();
if ($course_details) {
   if ($obj = $course_details->fetch_assoc()) {
      $result = $obj;
   }
}
?>

<?php include('includes/head.php') ?>
<section id="title" style="background-image: url(assets/images/home-hero.jpg)">
   <div class="container">
      <h1><?php echo $result['course_name'] ?></h1>
   </div>
</section>

<section class="in-sect">
   <div class="container">
      <div>

         <!-- Nav tabs -->
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Overview</a></li>
            <li role="presentation"><a href="#fees" aria-controls="fees" role="tab" data-toggle="tab">Fees</a></li>
            <li role="presentation"><a href="#apply" aria-controls="apply" role="tab" data-toggle="tab">How to Apply</a></li>
         </ul>

         <!-- Tab panes -->
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="overview">
               <div class="row">
                  <div class="col-md-8">
                     <p><?php echo $result['description'] ?></p>
                     <h3>How you'll learn</h3>
                     <p>Your learning experiences are designed to best suit the learning outcomes of the courses you choose.</p>
                     <div class="row">
                        <div class="col-md-6">
                           <ul>
                              <li><i class="fa fa-arrow-right">Lectures</i></li>
                              <li><i class="fa fa-arrow-right">Tutorials</i></li>
                              <li><i class="fa fa-arrow-right">Online study</i></li>
                              <li><i class="fa fa-arrow-right">Research experience</i></li>
                           </ul>
                        </div>
                        <div class="col-md-6">
                           <ul>
                              <li><i class="fa fa-arrow-right">Peer-assisted study sessions</i></li>
                              <li><i class="fa fa-arrow-right">Seminars</i></li>
                              <li><i class="fa fa-arrow-right">Workshops</i></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="c-form">
                        <form method="post" action="enroll-now.php" name="course_apply">
                           <h3>Heading Here</h3>
                           <div class="form-group">
                              <input type="hidden" name="course_id" value="<?php echo $result['course_id'] ?>" />
                           </div>
                           <div class="form-group">
                              <label>Email</label>
                              <input type="text" name="email" class="form-control">
                           </div>
                           <div class="form-group">
                              <label>Contact No</label>
                              <input type="text" name="contact_no" class="form-control">
                           </div>
                           <div class="form-group">
                              <button type="submit" name="course_apply" class="btn btn-info"> Enroll Now</button>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="fees">
               <div class="">
                  <h3>Indicative annual fee</h3>
                  <p class="fees">AUD $40,640</p>
                  <p>This indicative annual fee is the cost of enrolling into the course for the 2021 academic year. It is calculated based on a standard full-time study load (16 units per academic year).</p>
                  <p>If you enrol in a larger or smaller study load, your fees will be calculated on a proportionate basis. All fees are reviewed annually.</p>
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="apply">
               <h3>Applying online</h3>
               <p>All international applications should be submitted to UQ. If you prefer, you can use an approved UQ agent in your country.</p>
               <p>The program code for the Master of Advanced Economics is 5657.</p>

               <a href="enroll-now.php?Id=<?php echo $result['course_id'] ?>" class="btn btn-info">Apply Now</a>
            </div>
         </div>

      </div>
   </div>
</section>



<?php include('includes/foot.php') ?>
</body>

</html>