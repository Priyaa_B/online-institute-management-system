<?php
session_start();
require_once(dirname(__DIR__, 2) . "/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

//Add Classes------------------
if (isset($_POST['add_classes'])) {
   $_SESSION['msg_time'] = time();
   if ($con->query("INSERT INTO subject_classes SET course_id=" . $_POST['course_id'] . ", session_id='" . $_POST['session_id'] . "', session_subject_id='" . $_POST['session_subject_id'] . "', start_time='" . date('Y-m-d H:i:s',strtotime($_POST['start_time'])) . "', end_time='" . date('Y-m-d H:i:s',strtotime($_POST['end_time'])) . "' ")) {
      $class_id = $con->insert_id;
   } else {
      die("MySQL Error: " . $con->error);
   }

   if (!empty($class_id)) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
      $message = "Record cannot be added";
   }
   header('Location:index.php');
}

//Update Class------------------
if (isset($_POST['update_classes'])) {
   $_SESSION['msg_time'] = time();
   $id = $_POST['Id'];

   $result = $con->query("UPDATE subject_classes SET course_id=" . $_POST['course_id'] . ", session_id='" . $_POST['session_id'] . "', session_subject_id='" . $_POST['session_subject_id'] . "', start_time='" . date('Y-m-d H:i:s',strtotime($_POST['start_time'])) . "', end_time='" . date('Y-m-d H:i:s',strtotime($_POST['end_time'])) . "' WHERE class_id=" . $id);
   if ($result) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record updated successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be updated';
   }
   header('Location:index.php');
}

//Session Listing------------------
$sessions = array();
$result = $con->query("SELECT * FROM sessions");
if ($result) {
   while ($obj = $result->fetch_assoc()) {
      $sessions[] = $obj;
   }
}


//Edit Class---------------------
if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
   $result = $con->query("SELECT subject_classes.*,subjects.subject_name,courses.course_name FROM subject_classes INNER JOIN session_subjects ON session_subjects.session_subject_id = subject_classes.session_subject_id INNER JOIN subjects ON subjects.subject_id = session_subjects.subject_id INNER JOIN courses on courses.course_id = subjects.course_id WHERE class_id ='" . $_GET['Id'] . "'");
   if ($result) {
      if ($obj = $result->fetch_assoc()) {
         $editResult = $obj;
      }
   }
}
//courses listing
$courses = $con->query("SELECT * FROM courses");
$sessions = $con->query("SELECT * FROM sessions");
?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>
   <div class="form-heading">
      <div class="row">
         <div class="col-md-6">
            <h3>ADD CLASS</h3>
         </div>
         <div class="col-md-6 text-right">
            <a class="btn btn-warning" href="index.php"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
         </div>
      </div>
   </div>
   <form method="post">
      <div class="form-row">
         <div class="form-group col-md-12">
            <label for="course_id" class="col-form-label">Courses</label>
            <select id="course_id" name="course_id" class="form-control" required>
               <option value="">Choose course</option>
               <?php foreach ($courses as $course) { ?>
                  <option value="<?php echo $course['course_id'] ?>" <?php echo !empty($editResult['course_id']) && $editResult['course_id'] == $course['course_id'] ? 'selected' : '' ?>><?php echo $course['course_name'] ?></option>
               <?php } ?>
            </select>
         </div>
         

      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="session_id" class="col-form-label">Session</label>
            <select id="session_id" name="session_id" class="form-control" required>
               <option value="">Choose Session</option>
               <?php foreach ($sessions as $session) { ?>
                  <option value="<?php echo $session['session_id'] ?>" <?php echo !empty($editResult['session_id']) && $editResult['session_id'] == $session['session_id'] ? 'selected' : '' ?>><?php echo $session['session_name'] ?></option>
               <?php } ?>
            </select>
         </div>
         <div class="form-group col-md-6">
            <label for="session_subject_id" class="col-form-label">Subjects</label>
            <select id="session_subject_id" name="session_subject_id" class="form-control" required>
               <option value="">Choose Subjects</option>

            </select>
         </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="start_time">Start Time</label>
            <input type="datetime-local" class="form-control" name="start_time" id="start_time" value="<?php echo (isset($editResult['start_time']) && !empty($editResult['start_time']) ? date('Y-m-d\TH:i',strtotime($editResult['start_time'])) : '') ?>" placeholder="Start Time" required>
         </div>
          <div class="form-group col-md-6">
            <label for="end_time">End Time</label>
            <input type="datetime-local" class="form-control" name="end_time" id="end_time" value="<?php echo (isset($editResult['end_time']) && !empty($editResult['end_time']) ? date('Y-m-d\TH:i',strtotime($editResult['end_time'])) : '') ?>" placeholder="End Time" required>
         </div>
      </div>

      <div class="form-btn">
         <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) { ?>
            <input type="hidden" value="<?php echo $editResult['class_id'] ?>" name="Id" />
            <button type="submit" name="update_classes" class="btn btn-primary">Update</button>
         <?php } else { ?>
            <button type="submit" name="add_classes" class="btn btn-primary">Submit</button>
         <?php } ?>
         <button type="reset" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
<script>
$(document).ready(function(){
   if($('#course_id').val() == ""){
      $('#session_subject_id').attr('disabled',true);
      $('#session_id').attr('disabled',true);   
   }else{
      var course_id = $('#course_id').val();
      var session_id = $('#session_id').val();
      getSessionSubjects(session_id,course_id);
   }
   $('#session')
});
   $(document).on('change', '#course_id', function() {
   
      if($(this).val() != ""){
      $('#session_id').attr('disabled',false);
   $('#session_subject_id').attr('disabled',false);
   }else{
      $('#session_id').val('');
      $('#session_id').attr('disabled',true);
   $('#session_subject_id').val('');
   $('#session_subject_id').attr('disabled',true);
   }
});
   $(document).on('click', '#session_id', function() {
      var course_id = $("#course_id").val();
      if (course_id == "") {
         alert("Please select a course");
         return false;
      }
   })
   $(document).on('change', '#session_id', function() {
      var course_id = $("#course_id").val();
      var session_id = $(this).val();
      if (course_id == "") {
         alert("Please select a course");
         return false;
      }
      getSessionSubjects(session_id,course_id);

   });

   function getSessionSubjects(session_id,course_id){
      var seleted_id = "";
      var options ="";
      var user_id = "";
         if('<?php echo $_SESSION['user']['role_id']  ?>' == '<?php echo STAFF_ROLE_ID?>'){
         user_id = '<?php echo $_SESSION['user']['user_id'] ?>'
         }
      $.ajax({
         url: "getSessionSubjects.php",
         type: "Get",
         data: {
            session_id: session_id,
            course_id: course_id,
            user_id: user_id
         },
         success: function(response) {
            console.log(JSON.parse(response));
            $("#session_subject_id").html('');
            $("<option></option>",{value: "", text: "Choose Subjects"}).appendTo('#session_subject_id');
            $.map(JSON.parse(response), function(val, i) {
               $("<option></option>",{value: val.session_subject_id, text: val.subject_name}).appendTo('#session_subject_id');
            });
            if("<?php echo isset($editResult['session_subject_id']) ? $editResult['session_subject_id'] :"" ?>" != ""){
         selected_id = "<?php echo isset($editResult['session_subject_id']) ? $editResult['session_subject_id'] :"" ?>";
         $('select[id="session_subject_id"] option[value="'+selected_id+'"]').attr("selected","selected");
      }
            
         }

      });
   }

   $(document).ready(function(){
      var start_time = $('#start_time').val();
      if(start_time != ""){
         var min_date =  moment(start_time).format('YYYY-MM-DDTHH:mm'); 
     $('#end_time').attr('min',min_date);
      }
     
   })
   $(document).on('change',"#start_time",function(){
      var start_time = $(this).val();
     var min_date =  moment(start_time).format('YYYY-MM-DDTHH:mm'); 
     $('#end_time').attr('min',min_date);
   })
</script>
</body>

</html>