<?php
session_start();
require_once(dirname(__DIR__, 3) ."/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();


$where = 'WHERE 1=1';
if(!empty($_SESSION['user']) && $_SESSION['user']['role_id'] == STUDENT_ROLE_ID ){
   $student= $con->query("SELECT * FROM students WHERE user_id=".$_SESSION['user']['user_id']."");

   if($student){
       if($object = $student->fetch_assoc()){
           $student_detail = $object;
       }
   }

    $where .= ' AND student_assessments.student_id='.$student_detail['student_id'].' ';
}
$select_query = $con->query("SELECT student_assessments.*,assessments.assessment_no,assessments.total_marks,subjects.subject_name,students.student_id as std_id,students.first_name,students.last_name,grades.grade,documents.filename,sessions.session_name FROM student_assessments INNER JOIN students ON students.student_id = student_assessments.student_id INNER JOIN assessments ON assessments.assessment_id = student_assessments.assessment_id INNER JOIN session_subjects ON session_subjects.session_subject_id = assessments.session_subject_id
INNER JOIN sessions ON sessions.session_id = session_subjects.session_id INNER JOIN subjects ON subjects.subject_id = session_subjects.subject_id INNER JOIN grades ON grades.grade_id = student_assessments.grade_obtained INNER JOIN documents ON documents.reference_id = student_assessments.student_assessment_id AND documents.document_type =".ASSESSMENT_ANSWER." {$where}");
$records = array();
if($select_query){
   while($assessment = $select_query->fetch_assoc()){
      $records[] = $assessment;
   }
}



//Delete Record
if (isset($_GET['action']) && $_GET['action'] == 'delete'){
   
   $result = $con->query("DELETE FROM courses WHERE course_id='".$_GET['Id']."' ");

   $_SESSION['msg_time'] = time();
   if($result == true){
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record deleted successfully';
      header('Location:index.php');
   }else{
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be deleted';
      header('Location:index.php');
   }
}
$con->close();

?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
<?php include('../../includes/alert.php'); ?>
<div class="form-heading">
    <div class="row">
    <div class="col-md-6">
      <h3>ASSESSMENT RESULTS</h3>
    </div>
    </div>
   </div>
 		 <!--section-open-->
          <section class="table-listing">
            <div class="row">
               <div class="col-sm-12">
                  <div class="card">
                     <div class="table-responsive">
                        <table class="table">
                           <thead>
                              <tr>
                                 <th class="border-top-0">S. No.</th>
                                 <?php
                                     if($_SESSION['user']['role_id'] != STUDENT_ROLE_ID){                                 
                                 ?>
                                 <th class="border-top-0">Student</th>
                                 <?php } ?>
                                 <th class="border-top-0">Assessment</th>
                                 <th class="border-top-0">Subject</th>
                                 <th class="border-top-0">Session</th>
                                 <th class="border-top-0">Marks</th>
                                 <th class="border-top-0">Grade</th>
                              </tr>
                           </thead>
                           <tbody>
                               <?php if(isset($records) && count($records) > 0){
                                   $i = 1;
                                   foreach($records as $record){
                                   ?>
                                
                                <tr>
                                 <td><?php echo $i++; ?>
                                 </td>
                                 <?php
                                     if($_SESSION['user']['role_id'] != STUDENT_ROLE_ID){                                 
                                 ?>
                                 <td>
                                    <div class="display-flex">
                                       <h4><?php echo $record['first_name'].' '.$record['last_name'] ?></h4>
                                    </div>
                                 </td>
                                 <?php } ?>
                                 <td>
                                       <h4><?php echo $record['subject_name'] ?></h4>
                                 </td>
                                  <td>
                                       <h4><?php echo $record['assessment_no'] ?></h4>
                                 </td>
                                 <td>
                                       <h4><?php echo $record['session_name'] ?></h4>
                                 </td> 
                                 <td>
                                       <h4><?php echo $record['marks_obtained'].'/'.$record['total_marks'] ?></h4>
                                 </td>
                                  <td>
                                       <h4><?php echo $record['grade'] ?></h4>
                                 </td>
                              </tr>  
                                <?php }}else{ ?>
                                <tr class="text-center">
                                <td colspan="4">No Records Found</td>
                                </tr>
                              <?php } ?>

                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </section>
		 <!--section-closed-->
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
</body>

</html>