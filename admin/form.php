<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Admin Panel</title>
      <!-- Bootstrap -->
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600&display=swap" rel="stylesheet">
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">
   </head>
   <body>
	<!--header-->
      <header>
         <div class="col-md-6 col-sm-8 col-xs-8">
            <div class="col-md-3 col-sm-3 col-xs-4">
               <div class="logo">
                  <h3>Logo</h3>
               </div>
            </div>
            <div class="col-md-6 col-sm-9 col-xs-8">
               <div class="search-bar">
                  <input type="search" value="" placeholder="Search">
                  <div class="submit-btn">
                     <button><i class="fa fa-search" aria-hidden="true"></i></button>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-6 col-sm-4 col-xs-4">
			<div class="user">
				<div class="userimg">
					<img src="images/user.jpg">
				</div>
				<div class="username">
					<h3>Patrick <span><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3>
				</div>
				
				<div class="user-menu" style="display: none;">
					<ul>
						<li><a href="#">Edit Profile</a></li>
						<li><a href="#">Logout</a></li>
					</ul>
				</div>
				
			</div>
         </div>
      </header>
	  <!--header-->
	  
	  <!--left-side-menu-->
      <div class="left-side-menu">
         <ul class="menu-wrap">
            <li class="label-title">Navigation</li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li>
               <a href="#">
                  <i class="fa fa-home" aria-hidden="true"></i><span>Home</span>
                  <div class="arrow-right"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
               </a>
               <ul class="sub-menu">
                  <li><a href="#">Typography</a></li>
                  <li><a href="#">Typography</a></li>
                  <li><a href="#">Typography</a></li>
                  <li><a href="#">Typography</a></li>
               </ul>
            </li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
         </ul>
      </div>
	  <!--left-side-menu-->
	  
	  
	  <!--page-content-->
      <section class="page-content">
		 <div class="form-heading">
			<h3>Application Form</h3>
		 </div>
         <form>
            <div class="form-row">
               <div class="form-group col-md-6">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" placeholder="Email">
               </div>
               <div class="form-group col-md-6">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" placeholder="Password">
               </div>
            </div>
			<div class="form-row">
               <div class="form-group col-md-6">
                  <label for="email">Time</label>
                  <input type="time" class="form-control" id="time" placeholder="time">
               </div>
               <div class="form-group col-md-6">
                  <label for="Date">Date</label>
                  <input type="date" class="form-control" id="Date" placeholder="Date">
               </div>
            </div>
			<div class="form-row">
               <div class="form-group col-md-6">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" placeholder="Email">
               </div>
               <div class="form-group col-md-6">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" placeholder="Password">
               </div>
            </div>
            <div class="form-group">
               <label for="Address">Address</label>
               <input type="text" class="form-control" id="Address" placeholder="1234 Main St">
            </div>
            <div class="form-group">
               <label for="Address2">Address 2</label>
               <input type="text" class="form-control" id="Address2" placeholder="Apartment, studio, or floor">
            </div>
            <div class="form-row">
               <div class="form-group col-md-6">
                  <label for="city">City</label>
                  <input type="text" class="form-control" id="city">
               </div>
               <div class="form-group col-md-4">
                  <label for="inputState" class="col-form-label">State</label>
                  <select id="inputState" class="form-control">
                     <option>Choose</option>
                     <option>Option 1</option>
                     <option>Option 2</option>
                     <option>Option 3</option>
                  </select>
               </div>
               <div class="form-group col-md-2">
                  <label for="zip" class="col-form-label">Zip</label>
                  <input type="text" class="form-control" id="zip">
               </div>
            </div>
			<div class="form-group">
               <label for="uploader">File upload</label>
               <input type="file" class="form-control" id="uploader">
            </div>
           <div class="form-btn">
				<button type="submit" class="btn btn-primary">Submit</button>
				<button type="submit" class="btn btn-danger">Cancel</button>
		   </div>
         </form>
      </section>
	   <!--page-content-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script>
         jQuery(document).ready(function(){
         	jQuery('.left-side-menu ul li a').click(function(){
         		jQuery('.sub-menu').slideToggle('slow');
         	})
			
			jQuery('.user .username h3 span').click(function(){
				jQuery('.user-menu').slideToggle('slow');
			})
         })
          
      </script>
   </body>
</html>

