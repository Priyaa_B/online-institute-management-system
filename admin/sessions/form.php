<?php
session_start();
require_once(dirname(__DIR__, 2) ."/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

if (isset($_POST['add_session'])) {
   $_SESSION['msg_time'] = time();
   if ($con->query("INSERT INTO sessions SET  session_name='" . $_POST['session_name'] . "', started_at= '".date('Y-m-d',strtotime($_POST['started_at']))."' , ended_at= '".date('Y-m-d',strtotime($_POST['ended_at']))."' ")) {
      $session_id = $con->insert_id;
   }else {
      die("MySQL Error: " . $con->error);
   }


   if (!empty($session_id)) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
      $message = "Record cannot be added";
   }
   header('Location:index.php');
}

if (isset($_POST['update_session'])) {
   $_SESSION['msg_time'] = time();
   $id = $_POST['Id'];

   $result = $con->query('UPDATE sessions SET  session_name="' . $_POST['session_name'] . '", started_at= "'.date('Y-m-d',strtotime($_POST['started_at'])).'", ended_at= "'.date('Y-m-d',strtotime($_POST['ended_at'])).'" WHERE session_id=' . $id);
   if ($result) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record updated successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be updated';
   }
   header('Location:index.php');
}

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
   $result = $con->query("SELECT sessions.* FROM sessions WHERE session_id ='" . $_GET['Id'] . "'");
   if ($result) {
      if ($obj = $result->fetch_assoc()) {
         $editResult = $obj;
      }
   }
}

?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>
   <div class="form-heading">
      <div class="row">
         <div class="col-md-6">
            <h3>ADD SESSION</h3>
         </div>
         <div class="col-md-6 text-right">
            <a class="btn btn-warning" href="index.php"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
         </div>
      </div>
   </div>
   <form method="post">
      <div class="form-row">
         <div class="form-group col-md-12">
            <label for="session_name">Session Name</label>
            <input type="text" class="form-control" name="session_name" id="session_name" value="<?php echo (isset($editResult['session_name']) && !empty($editResult['session_name']) ? $editResult['session_name'] : '') ?>" placeholder="Session Name" required>
         </div>
         
      </div>
      <div class="form-row">
      <div class="form-group col-md-6">
            <label for="started_at"> Started At </label>
            <input type="date" class="form-control" name="started_at" id="started_at" value="<?php echo (isset($editResult['started_at']) && !empty($editResult['started_at']) ? $editResult['started_at'] : '') ?>" placeholder="Started At" required>
         </div>
          <div class="form-group col-md-6">
            <label for="ended_at"> Ended At </label>
            <input type="date" class="form-control" name="ended_at" id="ended_at" value="<?php echo (isset($editResult['ended_at']) && !empty($editResult['ended_at']) ? $editResult['ended_at'] : '') ?>" placeholder="Ended At" required>
         </div>
      </div>
      <div class="form-btn">
         <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) { ?>
            <input type="hidden" value="<?php echo $editResult['session_id'] ?>" name="Id" />
            <button type="submit" name="update_session" class="btn btn-primary">Update</button>
         <?php } else { ?>
            <button type="submit" name="add_session" class="btn btn-primary">Submit</button>
         <?php } ?>
         <button type="reset" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
</body>

</html>