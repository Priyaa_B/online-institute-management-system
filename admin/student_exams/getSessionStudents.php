<?php 
session_start();
require_once(dirname(__DIR__, 2) ."/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();
$result = $con->query("SELECT students.* FROM  students WHERE students.course_id = ".$_GET['course_id']." AND students.session_id=".$_GET['session_id']);
$exams = $con->query("SELECT * FROM student_exams LEFT JOIN documents ON documents.reference_id = student_exams.student_exam_id AND documents.document_type = ".EXAM_ANSWER." WHERE student_exams.exam_id=".$_GET['exam_id']);
$grades = $con->query("SELECT * FROM grades");
$records = array();
$exam_records = array();
$grade_records = array();
if ($result) {
    while ($obj = $result->fetch_assoc()) {
        $records[] = $obj;
    }
}
if ($exams) {
    while ($object = $exams->fetch_assoc()) {
        $exam_records[] = $object;
    }
}
if ($grades) {
    while ($object = $grades->fetch_assoc()) {
        $grade_records[] = $object;
    }
}
echo json_encode(['students' =>$records,'exams'=> $exam_records,'grades'=>$grade_records]);
