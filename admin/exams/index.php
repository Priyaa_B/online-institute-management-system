<?php
session_start();
require_once(dirname(__DIR__, 2) . "/database\conn.php");

$con = new DBConnection();
$con = $con->getdbconnect();
$file_path = EXAM_QUES_FILE_PATH;


$and = '';
if (isset($_GET['user_id']) && !empty($_GET['user_id'])) {
   $staff = $con->query("SELECT * FROM staffs WHERE user_id=" . $_GET['user_id'] . "");

   if ($staff) {
      if ($object = $staff->fetch_assoc()) {
         $staff_detail = $object;
      }
   }

   $and .= ' AND session_subjects.tutored_by=' . $staff_detail['staff_id'] . ' ';
}
//Get All Records
$select_query = "SELECT exams.*,subjects.subject_name,courses.course_name,documents.filename FROM exams INNER JOIN session_subjects ON session_subjects.session_subject_id = exams.session_subject_id {$and}
LEFT JOIN documents ON documents.reference_id = exams.exam_id AND documents.document_type =" . EXAM_QUESTION . " 
INNER JOIN subjects ON subjects.subject_id = session_subjects.subject_id INNER JOIN courses ON courses.course_id = subjects.course_id";
$dataQuery = $select_query;
$records = array();
$result = $con->query($dataQuery);
if ($result) {
   while ($obj = $result->fetch_assoc()) {
      $records[] = $obj;
   }
}

//Delete Record
if (isset($_GET['action']) && $_GET['action'] == 'delete') {
   $id =  $_GET['Id'];
   $result = $con->query("DELETE FROM exams WHERE exam_id='" . $_GET['Id'] . "' ");
   $check_file = $con->query("SELECT * FROM documents WHERE reference_id = " . $id);
   $file = array();
   if ($check_file) {
      while ($obj = $check_file->fetch_assoc()) {
         $file = $obj;
      }
   }
   $filename = $file_path . $file['filename'];
   if (file_exists($filename)) {
      unlink($filename);
      $output = array('status' => 'success');
   } else {
      $output = array('status' => 'fail');
   }
   $con->query("DELETE FROM documents WHERE reference_id = " . $id);

   $_SESSION['msg_time'] = time();
   if ($result == true) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record deleted successfully';
      header('Location:index.php');
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be deleted';
      header('Location:index.php');
   }
}
$con->close();
?>

<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>


<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>
   <div class="form-heading">
      <div class="row">
         <div class="col-md-6">
            <h3>EXAMS LISTING</h3>
         </div>
         <div class="col-md-6 text-right">
            <a class="btn btn-warning" href="form.php"><i class="fa fa-plus-circle mr-1" aria-hidden="true"></i> &nbsp; Add Exam</a>
         </div>
      </div>
   </div>
   <!--section-open-->
   <section class="table-listing">
      <div class="row">
         <div class="col-sm-12">
            <div class="card">
               <div class="table-responsive">
                  <table class="table">
                     <thead>
                        <tr>
                           <th class="border-top-0">S. No.</th>
                           <th class="border-top-0">Course</th>
                           <th class="border-top-0">Subject</th>
                           <th class="border-top-0">Exam Code</th>
                           <th class="border-top-0">Timings</th>
                           <th class="border-top-0">File</th>
                           <th class="border-top-0">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php if (isset($records) && count($records) > 0) {
                           $i = 1;
                           foreach ($records as $record) {
                        ?>

                              <tr>
                                 <td><?php echo $i++; ?>
                                 </td>
                                 <td>
                                    <div class="display-flex">
                                       <h4><?php echo $record['course_name'] ?></h4>
                                    </div>
                                 </td>
                                 <td><?php echo $record['subject_name'] ?></td>
                                 <td><?php echo $record['exam_code'] ?></td>
                                 <td><?php echo date('d-M-Y', strtotime($record['start_time'])) . ' | ' . date('H:i', strtotime($record['start_time'])) . ' - ' . date('H:i', strtotime($record['end_time'])) ?></td>

                                 <td><a href="<?php echo EXAM_QUES_FILE_DOWNLOAD_PATH . $record['filename'] ?>" download=""><i class="fa fa-file"></i></a></td>
                                 <td>
                                    <a class="label label-warning" href="form.php?action=edit&Id=<?php echo $record['exam_id'] ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a class="label label-warning" href="index.php?action=delete&Id=<?php echo $record['exam_id'] ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                 </td>
                              </tr>
                           <?php }
                        } else { ?>
                           <tr class="text-center">
                              <td colspan="7"> No Records Found</td>
                           </tr>
                        <?php } ?>


                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--section-closed-->
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
</body>

</html>