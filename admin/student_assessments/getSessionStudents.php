<?php 
session_start();
require_once(dirname(__DIR__, 2) ."/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();
$result = $con->query("SELECT students.* FROM  students WHERE students.course_id = ".$_GET['course_id']." AND students.session_id=".$_GET['session_id']);
$assessments = $con->query("SELECT * FROM student_assessments LEFT JOIN documents ON documents.reference_id = student_assessments.student_assessment_id AND documents.document_type = ".ASSESSMENT_ANSWER." WHERE student_assessments.assessment_id=".$_GET['assessment_id']);
$grades = $con->query("SELECT * FROM grades");
$records = array();
$assessment_records = array();
$grade_records = array();
if ($result) {
    while ($obj = $result->fetch_assoc()) {
        $records[] = $obj;
    }
}
if ($assessments) {
    while ($object = $assessments->fetch_assoc()) {
        $assessment_records[] = $object;
    }
}
if ($grades) {
    while ($object = $grades->fetch_assoc()) {
        $grade_records[] = $object;
    }
}
echo json_encode(['students' =>$records,'assessments'=> $assessment_records,'grades'=>$grade_records]);