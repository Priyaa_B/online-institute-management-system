<?php
session_start();
require_once(dirname(__DIR__, 2) . "/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

$file_path = ASSESSMENT_ANS_FILE_PATH;
//Add Student Assessments------------------
if (isset($_POST['add_std_assessment'])) {
   $_SESSION['msg_time'] = time();

   $con->query("DELETE FROM student_assessments WHERE assessment_id=" . $_POST['assessment_id']);
   foreach ($_POST['student_ids'] as $key => $student_id) {
      if ($con->query("INSERT INTO student_assessments SET assessment_id=" . $_POST['assessment_id'] . ", student_id=" . $student_id . ",submitted_at = '" . date('Y-m-d H:i:s', strtotime($_POST['submitted_at'][$student_id])) . "', marks_obtained=" . $_POST['marks_obtained'][$student_id] . ",grade_obtained=" . $_POST['grade_obtained'][$student_id] . "")) {
         $student_assessment_id = $con->insert_id;
      } else {
         $_SESSION['message_type'] = 'danger';
         $_SESSION['message'] = "Record cannot be added. Error: ". $con->error;
         header('Location:form.php');
         exit;
      }

      // Adding Files
      if (isset($_FILES['assessment_file']['name']) && count($_FILES['assessment_file']['name']) > 0 && !empty($_FILES['assessment_file']['name'][$student_id])) {
            if(isset($_POST['student_assessment_id']) && !empty($_POST['student_assessment_id'][$student_id])){
               $check_file = $con->query("SELECT * FROM documents WHERE reference_id=".$_POST['student_assessment_id'][$student_id]." AND document_type=".ASSESSMENT_ANSWER."");
               $file = array();
               if ($check_file) {
                  while ($obj = $check_file->fetch_assoc()) {
                     $file = $obj;
                  }
               }
               
               $filename = $file_path . $file['filename'];
               if (file_exists($filename)) {
                   unlink($filename);
                   $output = array('status' => 'success');
               } else {
                   $output = array('status' => 'fail');
               }
               $con->query("DELETE FROM documents WHERE reference_id=".$_POST['student_assessment_id'][$student_id]." AND document_type=".ASSESSMENT_ANSWER."");
            }
            $response = $con->query("INSERT INTO documents SET reference_id ='" . $student_assessment_id . "', filename='" . $student_assessment_id . '-' . $_FILES['assessment_file']['name'][$student_id] . "', document_type=" . ASSESSMENT_ANSWER);

            if (!file_exists($file_path)) {
               mkdir($file_path, 0777, true);
            }
            $question_file = $file_path . $student_assessment_id . '-' . basename($_FILES['assessment_file']['name'][$student_id]);

            if (move_uploaded_file($_FILES["assessment_file"]["tmp_name"][$student_id], $question_file)) {
               echo "The file " . htmlspecialchars(basename($_FILES['assessment_file']['name'][$student_id])) . "uploaded successfully.";
            } else {
               echo "Sorry, error occured while uploading file";
            }
         // }
      }else{

         if(isset($_POST['student_assessment_id']) && !empty($_POST['student_assessment_id'][$student_id])){
            $con->query("UPDATE documents SET reference_id =".$student_assessment_id." WHERE reference_id=".$_POST['student_assessment_id'][$student_id]." AND document_type=".ASSESSMENT_ANSWER." "); 
         }

      }


      $student_final_result_check = $con->query("SELECT * FROM student_subject_results WHERE session_subject_id=" . $_POST['session_subject_id'] . " AND student_id=" . $student_id . "");
      $student_final_result = array();
      if ($student_final_result_check) {
         while ($object = $student_final_result_check->fetch_assoc()) {
            $student_final_result[] = $object;
         }
      }
      if (count($student_final_result) > 0) {

         $assessments_result_check = $con->query("SELECT SUM(student_assessments.marks_obtained) as total_assessments_marks FROM student_assessments INNER JOIN assessments ON assessments.assessment_id = student_assessments.assessment_id AND assessments.session_subject_id = " . $_POST['session_subject_id'] . " WHERE student_assessments.student_id=" . $student_id . " ");
         $assessment_check = array();
         if ($assessments_result_check) {
            while ($obj = $assessments_result_check->fetch_assoc()) {
               $assessment_check = $obj;
            }
         }
       
            $final_assessment_marks = $assessment_check['total_assessments_marks'];
         
         
         $exams_result_check = $con->query("SELECT SUM(student_exams.marks_obtained) as total_exams_marks FROM student_exams INNER JOIN exams ON exams.exam_id = student_exams.exam_id AND exams.session_subject_id = " . $_POST['session_subject_id'] . " WHERE student_exams.student_id=" . $student_id . " ");

         $exam_check = array();
         if ($exams_result_check) {
            while ($obj = $exams_result_check->fetch_assoc()) {
               $exam_check = $obj;
            }
         }
       
         if (!empty($exam_check)) {
            $final_marks = $exam_check['total_exams_marks'] + $final_assessment_marks;
         } else {
            $final_marks = $final_assessment_marks;
         }
         if (!$con->query("UPDATE student_subject_results SET total_marks_obtained=" . $final_marks . ",overall_grade_obtained=" . $_POST['grade_obtained'][$student_id] . " WHERE session_subject_id=".$_POST['session_subject_id']." AND student_id=".$student_id." ")) {
            $_SESSION['message_type'] = 'danger';
            $_SESSION['message'] = "Record cannot be added. Error: " . $con->error;
         }else{
            $_SESSION['message_type'] = 'success';
         }
      } else {
         print_r('else');
         exit;
         $exams_result_check = $con->query("SELECT SUM(student_exams.marks_obtained) as total_exams_marks FROM student_exams INNER JOIN exams ON exams.exam_id = student_exams.exam_id AND exams.session_subject_id = " . $_POST['session_subject_id'] . " WHERE student_exams.student_id=" . $student_id . " ");

         $exam_check = array();
         if ($exams_result_check) {
            while ($obj = $exams_result_check->fetch_assoc()) {
               $exam_check = $obj;
            }
         }

         if (!empty($exam_check)) {
            $final_marks = $exam_check['total_exams_marks'] + $_POST['marks_obtained'][$student_id];
         } else {
            $final_marks = $_POST['marks_obtained'];
         }

         if (!$con->query("INSERT INTO student_subject_results SET session_id=" . $_POST['session_id'] . ",session_subject_id=" . $_POST['session_subject_id'] . ",student_id=" . $student_id . ",total_marks_obtained=" . $final_marks . ",overall_grade_obtained=" . $_POST['grade_obtained'][$student_id] . " ")) {
            $_SESSION['message_type'] = 'danger';
            $_SESSION['message'] = "Record cannot be added. Error: " . $con->error;
         }else{
            $_SESSION['message_type'] = 'success';
         }
      }
   }


   if ($_SESSION['message_type'] == 'success') {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
   }
   header('Location:form.php');
}


//Session Listing------------------
$sessions = array();
$result = $con->query("SELECT * FROM sessions");
if ($result) {
   while ($obj = $result->fetch_assoc()) {
      $sessions[] = $obj;
   }
}


//Edit Student---------------------
if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
   $result = $con->query("SELECT subject_classes.*,subjects.subject_name,courses.course_name FROM subject_classes INNER JOIN session_subjects ON session_subjects.session_subject_id = subject_classes.session_subject_id INNER JOIN subjects ON subjects.subject_id = session_subjects.subject_id INNER JOIN courses on courses.course_id = subjects.course_id WHERE class_id ='" . $_GET['Id'] . "'");
   if ($result) {
      if ($obj = $result->fetch_assoc()) {
         $editResult = $obj;
      }
   }
}

$courses = $con->query("SELECT * FROM courses");
?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>

   <div class="form-heading">
      <h3>Students Assessments</h3>
   </div>
   <div class="marks-form">
      <form method="post" enctype="multipart/form-data">
         <div class="row">
            <div class="col-md-6">
               <div class="subject-type">
                  <label>Course</label>
                  <select id="course_id" name="course_id" class="form-control" required>
                     <option value="">Choose course</option>
                     <?php foreach ($courses as $course) { ?>
                        <option value="<?php echo $course['course_id'] ?>" <?php echo !empty($editResult['course_id']) && $editResult['course_id'] == $course['course_id'] ? 'selected' : '' ?>><?php echo $course['course_name'] ?></option>
                     <?php } ?>
                  </select>
               </div>
            </div>
            <div class="col-md-6">
               <div class="subject-type">
                  <label>Session</label>
                  <select id="session_id" name="session_id" class="form-control" required>
                     <option value="">Choose session</option>
                     <?php foreach ($sessions as $session) { ?>
                        <option value="<?php echo $session['session_id'] ?>" <?php echo !empty($editResult['session_id']) && $editResult['session_id'] == $session['session_id'] ? 'selected' : '' ?>><?php echo $session['session_name'] ?></option>
                     <?php } ?>
                  </select>
               </div>
            </div>
         </div>
         <div class="row mt-3">
            <div class="col-md-6">
               <div class="subject-type">
                  <label>Subject</label>
                  <select id="session_subject_id" name="session_subject_id" class="form-control" required>
                     <option value="">Choose Subjects</option>
                  </select>
               </div>
            </div>
            <div class="col-md-6">
               <div class="subject-type">
                  <label>Assessment</label>
                  <select id="assessment_id" name="assessment_id" class="form-control" required>
                     <option value="">Choose Assessments</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="student-marks">
            <table class="table">
               <thead>
                  <tr>
                     <th class="border-top-0">Serial No</th>
                     <th class="border-top-0">Student Name</th>
                     <th class="border-top-0">Submitted At</th>
                     <th class="border-top-0">Marks Obtained</th>
                     <th class="border-top-0">Grades Obtained</th>
                     <th class="border-top-0">Attachment</th>
                  </tr>
               </thead>
               <tbody class="table-head">
                  <tr class="text-center">
                     <td colspan="6"> No Record Found</td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div class="form-btn text-center buttons">
            <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) { ?>
               <input type="hidden" value="<?php echo $editResult['class_id'] ?>" name="Id" />
               <button type="submit" name="update_std_attendances" class="btn btn-primary">Update</button>
            <?php } else { ?>
               <button type="submit" name="add_std_assessment" class="btn btn-primary">Submit</button>
            <?php } ?>
            <button type="reset" class="btn btn-danger">Cancel</button>
         </div>
      </form>
   </div>
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
<script>
   $(document).ready(function() {
      if ($('#course_id').val() == "") {
         $('#class_id').attr('disabled', true);
         $('.buttons').attr('hidden', true);
      } else {
         var course_id = $('#course_id').val();
         getSubjectClasses(course_id);
      }
      if ($('#session_id').val() == "") {
         $('#session_subject_id').attr('disabled', true);
      }
      if ($('#session_subject_id').val() == "") {
         $('#assessment_id').attr('disabled', true);
      }
   });
   $(document).on('change', '#course_id', function() {

      if ($(this).val() != "") {
         $(".submitted_at").val('');
         $(".marks_obtained").val('');
         $(".grades").val('');
         $(".assessment_file").val('');
         $('#session_id').val('');
         $('#session_subject_id option:not(:first)').remove();
         $('#assessment_id option:not(:first)').remove();
         $('#session_subject_id').attr('disabled', true);
         $('#assessment_id').attr('disabled', true);
      }
   });
   $(document).on('change', '#session_id', function() {
      var course_id = $('#course_id').val();
      var session_id = $(this).val();
      if (course_id == "") {
         alert("Please select course");
         $(this).val('');
         return false;
      }
      if (session_id != "") {
         $('#session_subject_id').attr('disabled', false);
         $(".submitted_at").val('');
         $(".marks_obtained").val('');
         $(".grades").val('');
         $(".assessment_file").val('');
         getSessionSubjects(session_id, course_id);
         $("#session_subject_id").trigger('change');
      } else {
         $('#session_subject_id').val('');
         $('#session_subject_id').attr('disabled', true);
      }
   });

   $(document).on('change', '#session_subject_id', function() {

      var session_subject_id = $(this).val();
      if (session_subject_id != "") {
         $("#assessment_id").attr('disabled', false);
      }
      $(".submitted_at").val('');
      $(".marks_obtained").val('');
      $(".grades").val('');
      $(".assessment_file").val('');

      getSessionSubjectAssessments(session_subject_id);

   });
   $(document).on('change', '#assessment_id', function() {
      var course_id = $('#course_id').val();
      var session_id = $('#session_id').val();
      var assessment_id = $(this).val();
      $(".submitted_at").val('');
      $(".marks_obtained").val('');
      $(".grades").val('');
      $(".assessment_file").val('');
      getSessionStudents(course_id, session_id,assessment_id);
   });

   function getSessionSubjects(session_id, course_id) {
      var seleted_id = "";
      var options = "";

      var user_id = "";
         if('<?php echo $_SESSION['user']['role_id']  ?>' == '<?php echo STAFF_ROLE_ID?>'){
         user_id = '<?php echo $_SESSION['user']['user_id'] ?>'
         }
      $.ajax({
         url: "getSessionSubjects.php",
         type: "Get",
         data: {
            session_id: session_id,
            course_id: course_id,
            user_id: user_id
         },
         success: function(response) {
            $("#session_subject_id").html('');
            $("<option></option>", {
               value: "",
               text: "Choose Subjects"
            }).appendTo('#session_subject_id');
            $.map(JSON.parse(response), function(val, i) {
               $("<option></option>", {
                  value: val.session_subject_id,
                  text: val.subject_name
               }).appendTo('#session_subject_id');
            });
            if ("<?php echo isset($editResult['session_subject_id']) ? $editResult['session_subject_id'] : "" ?>" != "") {
               selected_id = "<?php echo isset($editResult['session_subject_id']) ? $editResult['session_subject_id'] : "" ?>";
               $('select[id="session_subject_id"] option[value="' + selected_id + '"]').attr("selected", "selected");
            }

         }

      });
   }

   function getSessionSubjectAssessments(session_subject_id) {
      var seleted_id = "";

      var options = "";
      $.ajax({
         url: "getSessionSubjectAssessments.php",
         type: "Get",
         data: {
            session_subject_id: session_subject_id,
         },
         success: function(response) {
            console.log(JSON.parse(response));

            $("#assessment_id").html('');
            $("<option></option>", {
               value: "",
               text: "Choose Assessment"
            }).appendTo('#assessment_id');
            $.map(JSON.parse(response), function(val, i) {
               $("<option></option>", {
                  value: val.assessment_id,
                  text: val.assessment_no,
                  datamarks:val.total_marks
               }).appendTo('#assessment_id');
            });
            if ("<?php echo isset($editResult['assessment_id']) ? $editResult['assessment_id'] : "" ?>" != "") {
               selected_id = "<?php echo isset($editResult['assessment_id']) ? $editResult['assessment_id'] : "" ?>";
               $('select[id="assessment_id"] option[value="' + selected_id + '"]').attr("selected", "selected");
            }

         }

      });

   }

   function getSessionStudents(course_id, session_id,assessment_id = '') {
      var seleted_id = "";

      var tr = "";
      var marks = $("#assessment_id option:selected").attr('datamarks');
      $.ajax({
         url: "getSessionStudents.php",
         type: "Get",
         data: {
            course_id: course_id,
            session_id: session_id,
            assessment_id: assessment_id
         },
         success: function(response) {
            $inc = 1;
            $('.table-head').html('');

            $('.buttons').attr('hidden', false);
            var records = JSON.parse(response);

            // }
            
            $.map(records['students'], function(val, i) {
 
                  var assessments =[];
                  var required = "required";
                  var student_assessment_input = "";
                  var submitted_at = "";
                  var marks_obtained = "";
                  var grade_obtained = "";
                  var file = "";
                  if(records['assessments'].length > 0){
                     if(typeof records['assessments'][i] != "undefined"){
                    assessments = records['assessments'][i];
                    required = "";
                    student_assessment_input = '<input type ="hidden" value="'+assessments.student_assessment_id+'" name="student_assessment_id['+val.student_id+']"/>'
                    submitted_at = moment(assessments.submitted_at).format("YYYY-MM-DDTHH:mm");
                    marks_obtained = assessments.marks_obtained ;
                    grade_obtained = assessments.grade_obtained ;
                    if(assessments.filename != null){
                    file = '<div class="col-md-6"><a href="<?php echo ASSESSMENT_ANS_FILE_DOWNLOAD_PATH ?>'+assessments.filename+'" download><i class="fa fa-file"></i></a></div>';
                  }else{
                     file = "";
                  }
               }
               }
               var s = $("<select id='selectId' name='grade_obtained[" + val.student_id + "]' class='form-control grades' required/>");
               var options = 
               $("<option />", {
                  value: "",
                  text: "Choose Grade"
               }).appendTo(s);

               $.each( records['grades'], function( key, data ) {
                  console.log(grade_obtained);
              options =  $("<option />", {
                  value: data.grade_id,
                  text: data.grade,
                  
               });
               if(grade_obtained == data.grade_id){
                  options.attr('selected',true);
                  }
                 
               options.appendTo(s);

            });
              
            console.log(s );
               
               tr += ' <tr><td>' +
                  '<div class="display-flex">' +
                  '<h4>' + ($inc++) + '</h4>' +
                  '</div>' +
                  '</td>' +
                  '<td>' + val.first_name + ' ' + val.last_name + '</td>' +
                  '<td>'+student_assessment_input+'<input type="hidden" name="student_ids[]" value="' + val.student_id + '"/>' +
                  '<input type="datetime-local" class="form-control submitted_at" id="submitted_at" name="submitted_at[' + val.student_id + ']" value="'+submitted_at+'" required/></td>' +
                  '<td><div class="row float-left"><div class="col-md-6 pr-0"><input type="number" class="form-control marks_obtained" id="marks_obtained" max="'+marks+'" name="marks_obtained[' + val.student_id + ']" value="'+marks_obtained+'" required/></div><div class="col-md-5 text-left">/'+marks+'</div></div></td>' +
                  '<td>' + s.get(0).outerHTML + '</td>' +
                  '<td><div class="row"><div class="col-md-6"><input type="file" width="100px" name="assessment_file[' + val.student_id + ']" class="form-control assessment_file" '+required+'/></div>'+
               ''+file+'</div></td>' +
                  '</tr>';
            });
            $('.table-head').append(tr);
         }
      });
   }
</script>
</body>

</html>