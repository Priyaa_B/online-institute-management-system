
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Institute Management System</title>
   <link href="<?php echo PROJECT_BASE_PATH ?>/assets/css/bootstrap.css" rel="stylesheet">
   <link href="<?php echo PROJECT_BASE_PATH ?>/assets/css/style.css" rel="stylesheet">
   <link href="<?php echo PROJECT_BASE_PATH ?>/assets/css/fonts-googleapis.css" rel="stylesheet">
   <link href="<?php echo PROJECT_BASE_PATH ?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
</head>

<body>
   <!-- Website Header Starts-->
   <?php include('user_validation.php') ?>
   <header>
      <div class="col-md-6 col-sm-8 col-xs-6">
         <div class="col-md-3 col-sm-3 col-xs-8">
            <div class="toggle">
               <span></span>
               <span></span>
               <span></span>
            </div>
            <div class="logo">
               <img src="<?php echo PROJECT_BASE_PATH ?>/assets/images/Victorian-logo.png" style="height:50px;width:150px;">
            </div>
         </div>
         <!-- <div class="col-md-6 col-sm-9 col-xs-4 search-wrapper">
            <div class="search-bar">
               <input type="search" value="" placeholder="Search">
               <div class="submit-btn">
                  <button><i class="fa fa-search" aria-hidden="true"></i></button>
               </div>
            </div>
         </div> -->
      </div>
      <div class="col-md-6 col-sm-4 col-xs-6">
         <div class="user">
            <div class="userimg">
               <img src="<?php echo PROJECT_BASE_PATH ?>/assets/images/user.png">
            </div>
            <?php if(isset($_SESSION['user'])){ ?>
            <div class="username">
              
               <h3><?php echo $_SESSION['user']['name'] ?><span><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3>
            </div>

            <div class="user-menu" style="display: none;">
               <ul>
                  <li><a href="<?php echo PROJECT_BASE_PATH ?>/logout.php">Logout</a></li>
               </ul>
            </div>
<?php } ?>
         </div>
      </div>
   </header>
   <!-- Website Header Ends -->

   <!-- Webiste SideBarStarts -->
   <div class="left-side-menu">
      <ul class="menu-wrap">
         <li class="label-title">Navigation</li>


         <?php
         if($_SESSION['user']['role_id']==ADMIN_ROLE_ID){
         $active = "";
         $show = "";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_PATH . 'students/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'students/index.php')) {
            $active = 'active';
         } ?>
         <li><a href="<?php echo ADMIN_PATH ?>students/index.php" class="<?php echo $active ?>"><i class="fa fa-users" aria-hidden="true"></i><span>Students</span></a></li>

         <?php
         $active = "";
         $show = "";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_PATH . 'staff/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'staff/index.php')) {
            $active = 'active';
         } ?>
         <li><a href="<?php echo ADMIN_PATH ?>staff/index.php" class="<?php echo $active ?>"><i class="fa fa-users " aria-hidden="true"></i><span>Staff</span></a></li>


         <?php
         $active = "";
         $show = "display: none;";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_PATH . 'sessions/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'sessions/index.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'session_subjects/index.php')) {
            $active = 'active';
            $show = 'display: block;';
            $collapsed = 'collapsed';
         } ?>
         <li><a href="<?php echo ADMIN_PATH ?>sessions/index.php"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Sessions</span></a></li>

         <?php
         $active = "";
         $show = "";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_PATH . 'courses/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'courses/index.php')) {
            $active = 'active';
         } ?>
         <li><a href="<?php echo ADMIN_PATH ?>courses/index.php" class="<?php echo $active ?>"><i class="fa fa-graduation-cap" aria-hidden="true"></i><span>Courses</span></a></li>


         <?php
         $active = "";
         $show = "";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_PATH . 'subjects/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'subjects/index.php')) {
            $active = 'active';
         } ?>
         <li><a href="<?php echo ADMIN_PATH ?>subjects/index.php" class="<?php echo $active ?>"><i class="fa fa-book" aria-hidden="true"></i><span>Subjects</span></a></li>
       

         <?php
         $active = "";
         $show = "";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_PATH . 'grades/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'grades/index.php')) {
            $active = 'active';
         } ?>
         <li><a href="<?php echo ADMIN_PATH ?>grades/index.php" class="<?php echo $active ?>"><i class="fa fa-graduation-cap" aria-hidden="true"></i><span>Grades</span></a></li>
         <?php } ?>


         <?php
           if(in_array($_SESSION['user']['role_id'],[ADMIN_ROLE_ID,STAFF_ROLE_ID])){
         $active = "";
         $show = "display: none;";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_PATH . 'classes/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'classes/index.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'class_attendances/index.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'class_attendances/form.php')) {
            $active = 'active';
            $show = 'display: block;';
            $collapsed = 'collapsed';
         } ?>
        
         <li>
	              <a href="#tab2" id="tab2" class="<?php echo $active ?>">
				  <i class="fa fa-clock-o" aria-hidden="true"></i><span>Classes</span>
	                  <div class="arrow-right"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
	              </a>
	              <ul class="sub-menu tab2" style="<?php echo  $show  ?>">
	                  <li><a href="<?php echo ADMIN_PATH ?>classes/index.php" class="<?php echo (filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'classes/index.php' ? $active : '') ?>"><i class="fa fa-dot-circle-o 	" aria-hidden="true"></i>Classes</a></li>
	                  <li><a href="<?php echo ADMIN_PATH ?>class_attendances/form.php" class="<?php echo (filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'class_attendances/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'class_attendances/form.php' ? $active : '') ?>"><i class="fa fa-dot-circle-o 	" aria-hidden="true"></i>Attendances</a></li>
	           
	              </ul>
	          </li>

         <?php
         $active = "";
         $show = "display: none;";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_PATH . 'exams/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'exams/index.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'student_exams/index.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'student_exams/form.php')) {
            $active = 'active';
            $show = 'display: block;';
            $collapsed = 'collapsed';
         } ?>
         
         <li>
	              <a href="#tab3" id="tab3" class="<?php echo $active ?>">
				  <i class="fa fa-clock-o" aria-hidden="true"></i><span>Exams</span>
	                  <div class="arrow-right"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
	              </a>
	              <ul class="sub-menu tab3" style="<?php echo  $show  ?>">
	                  <li><a href="<?php echo ADMIN_PATH ?>exams/index.php" class="<?php echo (filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'exams/index.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'exams/form.php' ? $active : '') ?>"><i class="fa fa-dot-circle-o 	" aria-hidden="true"></i>Exams</a></li>
	                  <li><a href="<?php echo ADMIN_PATH ?>student_exams/form.php" class="<?php echo (filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'student_exams/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'student_exams/form.php' ? $active : '') ?>"><i class="fa fa-dot-circle-o 	" aria-hidden="true"></i>Student Exams</a></li>
	                
	              </ul>
	          </li>
             
             <?php
         $active = "";
         $show = "display: none;";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_PATH . 'assessments/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'assessments/index.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'student_assessments/index.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'student_assessments/form.php')) {
            $active = 'active';
            $show = 'display: block;';
            $collapsed = 'collapsed';
         } ?>
         
         <li>
	              <a href="#tab4" id="tab4" class="<?php echo $active ?>">
				  <i class="fa fa-clock-o" aria-hidden="true"></i><span>Assessments</span>
	                  <div class="arrow-right"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
	              </a>
	              <ul class="sub-menu tab4" style="<?php echo  $show  ?>">
	                  <li><a href="<?php echo ADMIN_PATH ?>assessments/index.php" class="<?php echo (filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'assessments/index.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'exams/form.php' ? $active : '') ?>"><i class="fa fa-dot-circle-o 	" aria-hidden="true"></i>Assessments</a></li>
	                  <li><a href="<?php echo ADMIN_PATH ?>student_assessments/form.php" class="<?php echo (filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'student_assessments/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'student_assessments/form.php' ? $active : '') ?>"><i class="fa fa-dot-circle-o 	" aria-hidden="true"></i>Student Assessments</a></li>
	                
	              </ul>
	          </li>
<?php } ?>

<?php

         $active = "";
         $show = "display: none;";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'student_panel/exams/index.php')) {
            $active = 'active';
            $show = 'display: block;';
            $collapsed = 'collapsed';
         } ?>
         
         <li><a href="<?php echo ADMIN_PATH ?>student_panel/exams/index.php" class="<?php echo $active ?>"><i class="fa fa-file-text" aria-hidden="true"></i><span>Exams Result</span></a></li>
     
     
     <?php

         $active = "";
         $show = "display: none;";
         if ((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL) == ADMIN_PATH . 'student_panel/assessments/index.php')) {
            $active = 'active';
            $show = 'display: block;';
            $collapsed = 'collapsed';
         } ?>
         
         <li><a href="<?php echo ADMIN_PATH ?>student_panel/assessments/index.php" class="<?php echo $active ?>"><i class="fa fa-file" aria-hidden="true"></i><span>Assessment Result</span></a></li>
     
     
      </ul>
   </div>
   <!-- Webiste SideBar Ends -->