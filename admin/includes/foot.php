<script src="<?php echo PROJECT_BASE_PATH?>/assets/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>  
<script src="<?php echo PROJECT_BASE_PATH?>/assets/js/bootstrap.js"></script>
<script>
      jQuery(document).ready(function(){
      jQuery('.left-side-menu ul li a').click(function(){
            
            var href = $(this).attr('id');
            jQuery('.sub-menu.'+href).slideToggle('slow');
      })
      
      jQuery('.user .username h3 span').click(function(){
      jQuery('.user-menu').slideToggle('slow');
      })
            
            jQuery('.toggle').click(function(){
            jQuery(".left-side-menu").toggleClass('active')
            });
      })
      
</script>

<script>
    $(function () {
        var message_type = sessionStorage.getItem("message_type");
        var message = sessionStorage.getItem("message");
        if (message_type && message) {
            var alert = '<div class="alert alert-'+message_type+' alert-dismissible show" role="alert">'+message+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            $('.alert-message-block').html(alert);
            sessionStorage.removeItem("message_type");
            sessionStorage.removeItem("message");
        }
    });
</script>