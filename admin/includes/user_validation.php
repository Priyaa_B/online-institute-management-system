<?php 
if(!isset($_SESSION['user']) || !in_array($_SESSION['user']['role_id'],[ADMIN_ROLE_ID,STAFF_ROLE_ID,STUDENT_ROLE_ID])){
      $loc = PROJECT_BASE_PATH.'/login.php';
      echo "<script>alert('Your are not authorized for this page')</script>";
      echo "<script>window.location='{$loc}'
				</script>";
}
