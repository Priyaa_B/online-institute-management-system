<?php
session_start();
require_once(dirname(__DIR__, 2) . "/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

//Add Assessment------------------
$file_path = ASSESSMENT_QUES_FILE_PATH;
if (isset($_POST['add_assessment'])) {
   $_SESSION['msg_time'] = time();

   if ($con->query("INSERT INTO assessments SET session_subject_id=" . $_POST['session_subject_id'] . ", assessment_no='" . $_POST['assessment_no'] . "', total_marks='" . $_POST['total_marks'] . "', assigned_at='" . date('Y-m-d H:i:s', strtotime($_POST['assigned_at'])) . "' ")) {
      $assessment_id = $con->insert_id;

      if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
         $response = $con->query("INSERT INTO documents SET reference_id ='" . $assessment_id . "', filename='" . $assessment_id . '-' . $_FILES['image']['name'] . "', document_type=" . ASSESSMENT_QUESTION);

         if (!file_exists($file_path)) {
            mkdir($file_path, 0777, true);
         }
         $question_file = $file_path . $assessment_id . '-' . basename($_FILES['image']['name']);

         if (move_uploaded_file($_FILES["image"]["tmp_name"], $question_file)) {
            echo "The file " . htmlspecialchars(basename($_FILES['image']['name'])) . "uploaded successfully.";
         } else {
            echo "Sorry, error occured while uploading file";
         }
      }
   } else {
      die("MySQL Error: " . $con->error);
   }

   if (!empty($assessment_id)) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
   }
   header('Location:index.php');
}

//Update Assessment------------------
if (isset($_POST['update_assessment'])) {
   $_SESSION['msg_time'] = time();
   $id = $_POST['Id'];

   $result = $con->query("UPDATE assessments SET session_subject_id=" . $_POST['session_subject_id'] . ", assessment_no='" . $_POST['assessment_no'] . "', total_marks='" . $_POST['total_marks'] . "', assigned_at='" . date('Y-m-d H:i:s', strtotime($_POST['assigned_at'])) . "' WHERE assessment_id=" . $id);


   if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {

      $check_file = $con->query("SELECT * FROM documents WHERE reference_id = " . $id, " AND document_type=" . ASSESSMENT_QUESTION);
      $file = array();
      if ($check_file) {
         while ($obj = $check_file->fetch_assoc()) {
            $file = $obj;
         }
      }
      $filename = $file_path . $file['filename'];
      if (file_exists($filename)) {
         unlink($filename);
         $output = array('status' => 'success');
      } else {
         $output = array('status' => 'fail');
      }
      $response = $con->query("UPDATE documents SET reference_id ='" . $id . "', filename='" . $id . '-' . $_FILES['image']['name'] . "' WHERE reference_id=" . $id . " AND document_type=" . ASSESSMENT_QUESTION);

      if (!file_exists($file_path)) {
         mkdir($file_path, 0777, true);
      }
      $question_file = $file_path . $id . '-' . basename($_FILES['image']['name']);

      if (move_uploaded_file($_FILES["image"]["tmp_name"], $question_file)) {
         echo "The file " . htmlspecialchars(basename($_FILES['image']['name'])) . "uploaded successfully.";
      } else {
         echo "Sorry, error occured while uploading file";
      }
   }

   if ($result) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record updated successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be updated';
   }
   header('Location:index.php');
}

//Session Listing------------------
$sessions = array();
$result = $con->query("SELECT * FROM sessions");
if ($result) {
   while ($obj = $result->fetch_assoc()) {
      $sessions[] = $obj;
   }
}


//Edit Assessment---------------------
if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
   $result = $con->query("SELECT assessments.*,session_subjects.session_subject_id,session_subjects.session_id,subjects.subject_name,courses.course_id,courses.course_name,documents.filename FROM assessments INNER JOIN session_subjects ON session_subjects.session_subject_id = assessments.session_subject_id 
   LEFT JOIN documents ON documents.reference_id = assessments.assessment_id AND documents.document_type =" . ASSESSMENT_QUESTION . " 
   INNER JOIN subjects ON subjects.subject_id = session_subjects.subject_id INNER JOIN courses ON courses.course_id = subjects.course_id WHERE assessment_id ='" . $_GET['Id'] . "'");
   if ($result) {
      if ($obj = $result->fetch_assoc()) {
         $editResult = $obj;
      }
   }
}
//courses listing
$courses = $con->query("SELECT * FROM courses");
$sessions = $con->query("SELECT * FROM sessions");
?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>
   <div class="form-heading">
      <div class="row">
         <div class="col-md-6">
            <h3>ADD ASSESSMENT</h3>
         </div>
         <div class="col-md-6 text-right">
            <a class="btn btn-warning" href="index.php"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
         </div>
      </div>
   </div>
   <form method="post" enctype="multipart/form-data">
      <div class="form-row">
         <div class="form-group col-md-12">
            <label for="course_id" class="col-form-label">Courses</label>
            <select id="course_id" name="course_id" class="form-control" required>
               <option value="">Choose course</option>
               <?php foreach ($courses as $course) { ?>
                  <option value="<?php echo $course['course_id'] ?>" <?php echo !empty($editResult['course_id']) && $editResult['course_id'] == $course['course_id'] ? 'selected' : '' ?>><?php echo $course['course_name'] ?></option>
               <?php } ?>
            </select>
         </div>


      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="session_id" class="col-form-label">Session</label>
            <select id="session_id" name="session_id" class="form-control" required>
               <option value="">Choose Session</option>
               <?php foreach ($sessions as $session) { ?>
                  <option value="<?php echo $session['session_id'] ?>" <?php echo !empty($editResult['session_id']) && $editResult['session_id'] == $session['session_id'] ? 'selected' : '' ?>><?php echo $session['session_name'] ?></option>
               <?php } ?>
            </select>
         </div>
         <div class="form-group col-md-6">
            <label for="session_subject_id" class="col-form-label">Subjects</label>
            <select id="session_subject_id" name="session_subject_id" class="form-control" required>
               <option value="">Choose Subjects</option>

            </select>
         </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="assessment_no" class="col-form-label">Assessment No.</label>
            <input type="text" class="form-control" name="assessment_no" id="assessment_no" value="<?php echo (isset($editResult['assessment_no']) && !empty($editResult['assessment_no']) ? $editResult['assessment_no'] : '') ?>" placeholder="Assessment No." required>
         </div>
         <div class="form-group col-md-6">
            <label for="total_marks" class="col-form-label">Total Marks</label>
            <input type="number" class="form-control" name="total_marks" id="total_marks" value="<?php echo (isset($editResult['total_marks']) && !empty($editResult['total_marks']) ? $editResult['total_marks'] : '') ?>" placeholder="Total Marks" required>
         </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="assigned_at">Assigned Time</label>
            <input type="datetime-local" class="form-control" name="assigned_at" id="assigned_at" value="<?php echo (isset($editResult['assigned_at']) && !empty($editResult['assigned_at']) ? date('Y-m-d\TH:i', strtotime($editResult['assigned_at'])) : '') ?>" placeholder="Assigned Time" required>
         </div>
         <div class="form-group col-md-6">
            <label for="uploader">File upload</label>
            <?php
            $required = "required";
            if (isset($editResult['filename']) && !empty($editResult['filename'])) {
               $required = "";
            } ?>
            <input type="file" name="image" class="form-control" id="uploader" <?php echo $required ?>>
            <?php if (isset($editResult['filename']) && !empty($editResult['filename'])) { ?>
               <div class="row">
                  <div class="col-md-12 mt-3">
                     <img class="img-thumbnail" src="<?php echo ASSESSMENT_QUES_FILE_DOWNLOAD_PATH . $editResult['filename'] ?>" width="200px" height="100px">
                  </div>
               </div>
            <?php } ?>
         </div>
      </div>
      <div class="form-btn">
         <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) { ?>
            <input type="hidden" value="<?php echo $editResult['assessment_id'] ?>" name="Id" />
            <button type="submit" name="update_assessment" class="btn btn-primary">Update</button>
         <?php } else { ?>
            <button type="submit" name="add_assessment" class="btn btn-primary">Submit</button>
         <?php } ?>
         <button type="reset" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
<script>
   $(document).ready(function() {
      if ($('#course_id').val() == "") {
         $('#session_subject_id').attr('disabled', true);
         $('#session_id').attr('disabled', true);
      } else {
         var course_id = $('#course_id').val();
         var session_id = $('#session_id').val();
         getSessionSubjects(session_id, course_id);
      }
   });
   $(document).on('change', '#course_id', function() {

      if ($(this).val() != "") {
         $('#session_id').attr('disabled', false);
         $('#session_subject_id').attr('disabled', false);
      } else {
         $('#session_id').val('');
         $('#session_id').attr('disabled', true);
         $('#session_subject_id').val('');
         $('#session_subject_id').attr('disabled', true);
      }
   });
   $(document).on('click', '#session_id', function() {
      var course_id = $("#course_id").val();
      if (course_id == "") {
         alert("Please select a course");
         return false;
      }
   })
   $(document).on('change', '#session_id', function() {
      var course_id = $("#course_id").val();
      var session_id = $(this).val();
      if (course_id == "") {
         alert("Please select a course");
         return false;
      }
      getSessionSubjects(session_id, course_id);

   });

   function getSessionSubjects(session_id, course_id) {
      var seleted_id = "";
      var options = "";
      var user_id = "";
      if ('<?php echo $_SESSION['user']['role_id']  ?>' == '<?php echo STAFF_ROLE_ID ?>') {
         user_id = '<?php echo $_SESSION['user']['user_id'] ?>'
      }
      $.ajax({
         url: "getSessionSubjects.php",
         type: "Get",
         data: {
            session_id: session_id,
            course_id: course_id,
            user_id: user_id
         },
         success: function(response) {
            console.log(JSON.parse(response));
            $("#session_subject_id").html('');
            $("<option></option>", {
               value: "",
               text: "Choose Subjects"
            }).appendTo('#session_subject_id');
            $.map(JSON.parse(response), function(val, i) {
               $("<option></option>", {
                  value: val.session_subject_id,
                  text: val.subject_name
               }).appendTo('#session_subject_id');
            });
            if ("<?php echo isset($editResult['session_subject_id']) ? $editResult['session_subject_id'] : "" ?>" != "") {
               selected_id = "<?php echo isset($editResult['session_subject_id']) ? $editResult['session_subject_id'] : "" ?>";
               $('select[id="session_subject_id"] option[value="' + selected_id + '"]').attr("selected", "selected");
            }

         }

      });
   }
</script>
</body>

</html>