<?php
session_start();
require_once(dirname(__DIR__, 2) . "/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

//Add Student------------------
if (isset($_POST['add_students'])) {
   $_SESSION['msg_time'] = time();

   if ($con->query("INSERT INTO users SET role_id=" . STUDENT_ROLE_ID . ", name='" . $_POST['first_name'] . "', username='" . $_POST['username'] . "',password='" . Encrypt($_POST['password']) . "' ")) {
      $user_id = $con->insert_id;
   } else {
      die("MySQL Error: " . $con->error);
   }

   if ($con->query("INSERT INTO students SET user_id=" . $user_id . ", first_name='" . $_POST['first_name'] . "', last_name='" . $_POST['last_name'] . "', father_name='" . $_POST['father_name'] . "', contact_no='" . $_POST['contact_no'] . "',dob='" . date('Y-m-d', strtotime($_POST['dob'])) . "',course_id='" . $_POST['course_id'] . "',session_id='" . $_POST['session_id'] . "',email='" . $_POST['email'] . "',address='" . $_POST['address'] . "' ")) {
      $student_id = $con->insert_id;
   } else {
      die("MySQL Error: " . $con->error);
   }

   if (!empty($student_id)) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
      $message = "Record cannot be added";
   }
   header('Location:index.php');
}

//Update Student------------------
if (isset($_POST['update_students'])) {
   $_SESSION['msg_time'] = time();
   $id = $_POST['Id'];

   $user_check= $con->query("SELECT * FROM students WHERE student_id=".$id."");
   $user = array();
   if($user_check){
      if($std = $user_check->fetch_assoc()){
             $user = $std;
      }
   }

   if(empty($user['user_id'])){
      $con->query("INSERT INTO users SET name='".$_POST['first_name']."',role_id=".STUDENT_ROLE_ID.",username='".$_POST['username']."',password='".Encrypt($_POST['password'])."'");
      $user_id = $con->insert_id;
      $con->query("UPDATE students SET user_id=".$user_id." WHERE student_id=".$id."");
   }
   $result = $con->query('UPDATE students SET  first_name="' . $_POST['first_name'] . '", last_name="' . $_POST['last_name'] . '", father_name="' . $_POST['father_name'] . '", contact_no="' . $_POST['contact_no'] . '",dob="' . date('Y-m-d', strtotime($_POST['dob'])) . '",course_id="' . $_POST['course_id'] . '",session_id="' . $_POST['session_id'] . '",address="' . $_POST['address'] . '",email="' . $_POST['email'] . '" WHERE student_id=' . $id);
   if ($result) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record updated successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be updated';
   }
   header('Location:index.php');
}

//Session Listing------------------
$sessions = array();
$result = $con->query("SELECT * FROM sessions");
if ($result) {
   while ($obj = $result->fetch_assoc()) {
      $sessions[] = $obj;
   }
}

//Courses Listing-------------------
$courses = $con->query("SELECT * FROM courses");



//Edit Student---------------------
if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
   $result = $con->query("SELECT students.*,users.* FROM students LEFT JOIN users on users.user_id = students.user_id WHERE student_id ='" . $_GET['Id'] . "'");
   if ($result) {
      if ($obj = $result->fetch_assoc()) {
         $editResult = $obj;
      }
   }
}
?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>
   <div class="form-heading">
      <div class="row">
         <div class="col-md-6">
            <h3>ADD STUDENT</h3>
         </div>
         <div class="col-md-6 text-right">
            <a class="btn btn-warning" href="index.php"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
         </div>
      </div>
   </div>
   <form method="post">
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo (isset($editResult['first_name']) && !empty($editResult['first_name']) ? $editResult['first_name'] : '') ?>" placeholder="First Name" required>
         </div>
         <div class="form-group col-md-6">
            <label for="last_name">Last Name</label>
            <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo (isset($editResult['last_name']) && !empty($editResult['last_name']) ? $editResult['last_name'] : '') ?>" placeholder="Last Name" required>
         </div>

      </div>
      <div class="form-row">

         <div class="form-group col-md-6">
            <label for="father_name">Father Name</label>
            <input type="text" class="form-control" name="father_name" id="father_name" value="<?php echo (isset($editResult['father_name']) && !empty($editResult['father_name']) ? $editResult['father_name'] : '') ?>" placeholder="Father Name" required>
         </div>
         <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" value="<?php echo (isset($editResult['email']) && !empty($editResult['email']) ? $editResult['email'] : '') ?>" placeholder="email" required>
         </div>
      </div>
      <div class="form-row">

         <div class="form-group col-md-6">
            <label for="contact_no">Contact No</label>
            <input type="text" class="form-control" name="contact_no" id="contact_no" value="<?php echo (isset($editResult['contact_no']) && !empty($editResult['contact_no']) ? $editResult['contact_no'] : '') ?>" placeholder="Contact No" required>
         </div>
         <div class="form-group col-md-6">
            <label for="dob">DOB</label>
            <input type="date" class="form-control" name="dob" id="dob" value="<?php echo (isset($editResult['dob']) && !empty($editResult['dob']) ? $editResult['dob'] : '') ?>" placeholder="DOB" required>
         </div>
      </div>
      <div class="form-row">

         <div class="form-group col-md-6">
            <label for="course_id" class="col-form-label">Courses</label>
            <select id="course_id" name="course_id" class="form-control" required>
               <option>Choose Course</option>
               <?php foreach ($courses as $course) { ?>
                  <option value="<?php echo $course['course_id'] ?>" <?php echo  !empty($editResult['course_id']) && $editResult['course_id'] == $course['course_id'] ? "selected" : "" ?>><?php echo $course['course_name'] ?></option>
               <?php } ?>
            </select>
         </div>
         <div class="form-group col-md-6">
            <label for="session_id" class="col-form-label">Session</label>
            <select id="session_id" name="session_id" class="form-control" required>
               <option>Choose session</option>
               <?php foreach ($sessions as $session) { ?>
                  <option value="<?php echo $session['session_id'] ?>" <?php echo  !empty($editResult['session_id']) && $editResult['session_id'] == $session['session_id'] ? "selected" : "" ?>><?php echo $session['session_name'] ?></option>
               <?php } ?>
            </select>
         </div>
      </div>
      <?php if ( empty($editResult['user_id'])) { ?>
         <div class="form-row">
            <div class="form-group col-md-6">
               <label for="username">Username</label>
               <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
            </div>
            <div class="form-group col-md-6">
               <label for="password">Password</label>
               <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
            </div>
         </div>
      <?php } ?>
      <div class="form-group">
         <label for="sddress">Address</label>
         <textarea class="form-control" id="address" name="address" rows="3" required><?php echo (isset($editResult['address']) && !empty($editResult['address']) ? $editResult['address'] : '') ?></textarea>
      </div>
      <div class="form-btn">
         <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) { ?>
            <input type="hidden" value="<?php echo $editResult['student_id'] ?>" name="Id" />
            <button type="submit" name="update_students" class="btn btn-primary">Update</button>
         <?php } else { ?>
            <button type="submit" name="add_students" class="btn btn-primary">Submit</button>
         <?php } ?>
         <button type="reset" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
</body>

</html>