<?php
session_start();
require_once(dirname(__DIR__, 2) ."/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

if (isset($_POST['add_staff'])) {
   $_SESSION['msg_time'] = time();


   if ($con->query("INSERT INTO users SET role_id=" . STAFF_ROLE_ID . ", name='" . $_POST['first_name'] . "', username='" . $_POST['username'] . "',password='" . Encrypt($_POST['password']) . "' ")) {
      $user_id = $con->insert_id;
   }else {
      die("MySQL Error: " . $con->error);
   }

   if ($con->query("INSERT INTO staffs SET user_id=" . $user_id . ", first_name='" . $_POST['first_name'] . "', last_name='" . $_POST['last_name'] . "', contact_no='" . $_POST['contact_no'] . "',age='" . $_POST['age'] . "',gender='" . $_POST['gender'] . "',email='" . $_POST['email'] . "',address='" . $_POST['address'] . "' ")) {
      $staff_id = $con->insert_id;
   }else {
      die("MySQL Error: " . $con->error);
   }

   if (!empty($staff_id)) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
      $message = "Record cannot be added";
   }
   header('Location:index.php');
}

if (isset($_POST['update_staff'])) {
   $_SESSION['msg_time'] = time();
   $id = $_POST['Id'];
   $result = $con->query('UPDATE staffs SET  first_name="' . $_POST['first_name'] . '", last_name="' . $_POST['last_name'] . '", contact_no="' . $_POST['contact_no'] . '",age="' . $_POST['age'] . '",gender="' . $_POST['gender'] . '",address="' . $_POST['address'] . '",email="' . $_POST['email'] . '" WHERE staff_id=' . $id);
   if ($result == true) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record updated successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be updated';
   }
   header('Location:index.php');
}

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
   $result = $con->query("SELECT staffs.* FROM staffs INNER JOIN users on users.user_id = staffs.user_id WHERE staff_id ='" . $_GET['Id'] . "'");
   if ($result) {
      if ($obj = $result->fetch_assoc()) {
         $editResult = $obj;
      }
   }
}

?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>
   <div class="form-heading">
      <div class="row">
         <div class="col-md-6">
            <h3>ADD STAFF</h3>
         </div>
         <div class="col-md-6 text-right">
            <a class="btn btn-warning" href="index.php"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
         </div>
      </div>
   </div>
   <form method="post">
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo (isset($editResult['first_name']) && !empty($editResult['first_name']) ? $editResult['first_name'] : '') ?>" placeholder="First Name" required>
         </div>
         <div class="form-group col-md-6">
            <label for="last_name">Last Name</label>
            <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo (isset($editResult['last_name']) && !empty($editResult['last_name']) ? $editResult['last_name'] : '') ?>" placeholder="Last Name" required>
         </div>

      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" value="<?php echo (isset($editResult['email']) && !empty($editResult['email']) ? $editResult['email'] : '') ?>" placeholder="email" required>
         </div>
         <div class="form-group col-md-6">
            <label for="contact_no">Contact No</label>
            <input type="text" class="form-control" name="contact_no" id="contact_no" value="<?php echo (isset($editResult['contact_no']) && !empty($editResult['contact_no']) ? $editResult['contact_no'] : '') ?>" placeholder="Contact No" required>
         </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="age">Age</label>
            <input type="number" class="form-control" name="age" id="age" value="<?php echo (isset($editResult['age']) && !empty($editResult['age']) ? $editResult['age'] : '') ?>" placeholder="Age" required>
         </div>
         <div class="form-group col-md-6">
            <label for="gender">Gender</label>
            <input type="text" class="form-control" name="gender" id="gender" value="<?php echo (isset($editResult['gender']) && !empty($editResult['gender']) ? $editResult['gender'] : '') ?>" placeholder="Gender" required>
         </div>
      </div>
      <?php if (!isset($editResult) || empty($editResult)) { ?>
         <div class="form-row">
            <div class="form-group col-md-6">
               <label for="username">Username</label>
               <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
            </div>
            <div class="form-group col-md-6">
               <label for="password">Password</label>
               <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
            </div>
         </div>
      <?php } ?>
      <div class="form-group">
         <label for="sddress">Address</label>
         <textarea class="form-control" id="address" name="address" rows="3" required><?php echo (isset($editResult['address']) && !empty($editResult['address']) ? $editResult['address'] : '') ?></textarea>
      </div>
      <div class="form-btn">
         <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) { ?>
            <input type="hidden" value="<?php echo $editResult['staff_id'] ?>" name="Id" />
            <button type="submit" name="update_staff" class="btn btn-primary">Update</button>
         <?php } else { ?>
            <button type="submit" name="add_staff" class="btn btn-primary">Submit</button>
         <?php } ?>
         <button type="reset" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
</body>

</html>