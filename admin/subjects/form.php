<?php
session_start();
require_once(dirname(__DIR__, 2) ."/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

if (isset($_POST['add_course'])) {
   $_SESSION['msg_time'] = time();
   if($con->query("INSERT INTO subjects SET subject_name='" . $_POST['subject_name'] . "',description='" . ReplaceSql($_POST['description']) . "',subject_code='" . $_POST['subject_code'] . "' ,course_id='" . $_POST['course_id'] . "' ")){
      $subject_id = $con->insert_id;
   }else{
      die("MySQL Error: " . $con->error);
   }
   if($con->query("INSERT INTO session_subjects SET session_id='" . $_POST['session_id'] . "',subject_id=" . $subject_id. ",tutored_by='" . $_POST['tutored_by']. "',total_classes='" . $_POST['total_classes']. "',total_assessments='" . $_POST['total_assessments']. "' ")){
      $session_subject_id = $con->insert_id;
   }
   else{
      die("MySQL Error: " . $con->error);
   }
   if (!empty($subject_id)) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
      $message = "Record cannot be added";
   }
   header('Location:index.php');
}

if (isset($_POST['update_course'])) {
   $_SESSION['msg_time'] = time();
   $id = $_POST['Id'];
   $subject_update = 'UPDATE subjects SET  subject_name="' . $_POST['subject_name'] . '",subject_code="' . $_POST['subject_code'] . '",description="' . $_POST['description'] . '",course_id="' . $_POST['course_id'] . '" WHERE subject_id=' . $id;
   $session_subject_id = "UPDATE session_subjects SET session_id='" . $_POST['session_id'] . "',subject_id=" . $id. ",tutored_by='" . $_POST['tutored_by']. "',total_classes='" . $_POST['total_classes']. "',total_assessments='" . $_POST['total_assessments']. "' WHERE subject_id=".$id;
   $result = $con->query($subject_update);
   $session_subject_result = $con->query($session_subject_id);
   if ($result == true) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record updated successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be updated';
   }
   header('Location:index.php');
}

$courses = $con->query("SELECT * FROM courses");
$sessions = $con->query("SELECT * FROM sessions");
$staffs = $con->query("SELECT * FROM staffs");

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
   $result  = array();
   $result = $con->query("SELECT subjects.*,courses.course_name,session_subjects.session_id,session_subjects.tutored_by,session_subjects.total_classes,session_subjects.total_assessments FROM subjects LEFT JOIN courses ON courses.course_id = subjects.course_id LEFT JOIN session_subjects ON session_subjects.subject_id = subjects.subject_id WHERE subjects.subject_id ='" . $_GET['Id'] . "'");
   if ($result) {
      if ($obj = $result->fetch_assoc()) {
         $editResult = $obj;
      }
   }
}

?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>
   <div class="form-heading">
      <div class="row">
         <div class="col-md-6">
            <h3>ADD SUBJECT</h3>
         </div>
         <div class="col-md-6 text-right">
            <a class="btn btn-warning" href="index.php"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
         </div>
      </div>
   </div>
   <form method="post">
      <div class="form-row">
         <div class="form-group col-md-4">
            <label for="course_id" class="col-form-label">Courses</label>
            <select id="course_id" name="course_id" class="form-control" required>
               <option>Choose course</option>
               <?php foreach ($courses as $course) { ?>
                  <option value="<?php echo $course['course_id'] ?>" <?php echo !empty($editResult['course_id']) && $editResult['course_id'] == $course['course_id'] ? 'selected' :'' ?>><?php echo $course['course_name'] ?></option>
               <?php } ?>
            </select>
         </div>
         <div class="form-group col-md-4">
            <label for="session_id" class="col-form-label">Session</label>
            <select id="session_id" name="session_id" class="form-control" required>
               <option>Choose Session</option>
               <?php foreach ($sessions as $session) { ?>
                  <option value="<?php echo $session['session_id'] ?>" <?php echo !empty($editResult['session_id']) && $editResult['session_id'] == $session['session_id'] ? 'selected' :'' ?>><?php echo $session['session_name'] ?></option>
               <?php } ?>
            </select>
         </div>
         <div class="form-group col-md-4">
            <label for="subject_name">Subject Name</label>
            <input type="text" class="form-control" name="subject_name" id="subject_name" value="<?php echo (isset($editResult['subject_name']) && !empty($editResult['subject_name']) ? $editResult['subject_name'] : '') ?>" placeholder="Subject Name" required>
         </div>
      
      </div>
      <div class="form-row">
    
         <div class="form-group col-md-6">
            <label for="subject_code">Subject Code</label>
            <input type="text" class="form-control" name="subject_code" id="subject_code" value="<?php echo (isset($editResult['subject_code']) && !empty($editResult['subject_code']) ? $editResult['subject_code'] : '') ?>" placeholder="Subject code" required>
         </div>
         <div class="form-group col-md-6">
         <label for="tutored_by" class="col-form-label">Tutor</label>
            <select id="tutored_by" name="tutored_by" class="form-control">
               <option>Choose tutor</option>
               <?php foreach ($staffs as $staff) { ?>
                  <option value="<?php echo $staff['staff_id'] ?>" <?php echo !empty($editResult['tutored_by']) && $editResult['tutored_by'] == $staff['staff_id'] ? 'selected' :'' ?>><?php echo $staff['first_name'].' '.$staff['last_name'] ?></option>
               <?php } ?>
            </select>
         </div>
      </div>
        <div class="form-row">
      <div class="form-group col-md-6">
            <label for="total_classes">Total Classes</label>
            <input type="text" class="form-control" name="total_classes" id="total_classes" value="<?php echo (isset($editResult['total_classes']) && !empty($editResult['total_classes']) ? $editResult['total_classes'] : '') ?>" placeholder="Total Cases" required>
         </div>
      <div class="form-group col-md-6">
            <label for="total_assessments">Total Assessments</label>
            <input type="text" class="form-control" name="total_assessments" id="total_assessments" value="<?php echo (isset($editResult['total_assessments']) && !empty($editResult['total_assessments']) ? $editResult['total_assessments'] : '') ?>" placeholder="Total Assessments" required>
         </div>
         
      </div>
      <div class="form-group">
      <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" rows="3" required><?php echo (isset($editResult['description']) && !empty($editResult['description']) ? $editResult['description'] : '') ?></textarea>
      </div>

      <div class="form-btn">
         <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) { ?>
            <input type="hidden" value="<?php echo $editResult['subject_id'] ?>" name="Id" />
            <button type="submit" name="update_course" class="btn btn-primary">Update</button>
         <?php } else { ?>
            <button type="submit" name="add_course" class="btn btn-primary">Submit</button>
         <?php } ?>
         <button type="reset" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
</body>

</html>