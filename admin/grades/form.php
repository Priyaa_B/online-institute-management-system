<?php
session_start();
require_once(dirname(__DIR__, 2) ."/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

if (isset($_POST['add_grade'])) {
   $_SESSION['msg_time'] = time();   
   if ($con->query("INSERT INTO grades SET grade='". $_POST['grade'] ."',criteria='". $_POST['criteria'] ."' ")) {
      $grade_id = $con->insert_id;
   }else {
      die("MySQL Error: " . $con->error);
   }

   if (!empty($grade_id)) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
      $message = "Record cannot be added";
   }
   header('Location:index.php');
}

if (isset($_POST['update_grade'])) {
   $_SESSION['msg_time'] = time();
   $id = $_POST['Id'];
   $result = $con->query('UPDATE grades SET  grade="' . $_POST['grade'] . '",criteria="' . $_POST['criteria'] . '" WHERE grade_id=' . $id);
   if ($result) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record updated successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be updated';
   }
   header('Location:index.php');
}

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
   $result = $con->query("SELECT grades.* FROM grades WHERE grade_id ='" . $_GET['Id'] . "'");
   if ($result) {
      if ($obj = $result->fetch_assoc()) {
         $editResult = $obj;
      }
   }
}

?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>
   <div class="form-heading">
      <div class="row">
         <div class="col-md-6">
            <h3>ADD GRADE</h3>
         </div>
         <div class="col-md-6 text-right">
            <a class="btn btn-warning" href="index.php"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
         </div>
      </div>
   </div>
   <form method="post">
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="grade">Grade Name</label>
            <input type="text" class="form-control" name="grade" id="grade" value="<?php echo (isset($editResult['grade']) && !empty($editResult['grade']) ? $editResult['grade'] : '') ?>" placeholder="Grade Name" required>
         </div>
         <div class="form-group col-md-6">
            <label for="criteria">Criteria</label>
            <input type="text" class="form-control" name="criteria" id="criteria" value="<?php echo (isset($editResult['criteria']) && !empty($editResult['criteria']) ? $editResult['criteria'] : '') ?>" placeholder="criteria" required>
         </div>
         
      </div>
     
      <div class="form-btn">
         <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) { ?>
            <input type="hidden" value="<?php echo $editResult['grade_id'] ?>" name="Id" />
            <button type="submit" name="update_grade" class="btn btn-primary">Update</button>
         <?php } else { ?>
            <button type="submit" name="add_grade" class="btn btn-primary">Submit</button>
         <?php } ?>
         <button type="reset" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
</body>

</html>