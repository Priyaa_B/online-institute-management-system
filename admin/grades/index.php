<?php
session_start();
require_once(dirname(__DIR__, 2) . "/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

$where_condition = '';
//Get all records
$select_query = 'SELECT grades.* FROM grades ';

$dataQuery = $select_query . $where_condition;
$result = $con->query($dataQuery);
if ($result) {
   while ($obj = $result->fetch_assoc()) {
      $records[] = $obj;
   }
}



//Delete Record
if (isset($_GET['action']) && $_GET['action'] == 'delete') {

   $result = $con->query("DELETE FROM grades WHERE grade_id='" . $_GET['Id'] . "' ");

   $_SESSION['msg_time'] = time();
   if ($result == true) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record deleted successfully';
      header('Location:index.php');
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be deleted';
      header('Location:index.php');
   }
}
$con->close();

?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>
   <div class="form-heading">
      <div class="row">
         <div class="col-md-6">
            <h3>GRADE LISTING</h3>
         </div>
         <div class="col-md-6 text-right">
            <a class="btn btn-warning" href="form.php"><i class="fa fa-plus-circle mr-1" aria-hidden="true"></i> &nbsp; Add Grade</a>
         </div>
      </div>
   </div>
   <!--section-open-->
   <section class="table-listing">
      <div class="row">
         <div class="col-sm-12">
            <div class="card">
               <div class="table-responsive">
                  <table class="table">
                     <thead>
                        <tr>
                           <th class="border-top-0">S. No.</th>
                           <th class="border-top-0">Grade</th>
                           <th class="border-top-0">Criteria</th>
                           <th class="border-top-0">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php if (isset($records) && count($records) > 0) {
                           $i = 1;
                           foreach ($records as $record) {
                        ?>

                              <tr>
                                 <td><?php echo $i++; ?>
                                 </td>
                                 <td>
                                    <div class="display-flex">
                                       <h4><?php echo $record['grade'] ?></h4>
                                    </div>
                                 </td>
                                 <td>
                                    <h4><?php echo $record['criteria'] ?></h4>
                                 </td>
                                 <td>
                                    <a class="label label-warning" href="form.php?action=edit&Id=<?php echo $record['grade_id'] ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a class="label label-warning" href="index.php?action=delete&Id=<?php echo $record['grade_id'] ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                 </td>
                              </tr>
                           <?php }
                        } else { ?>
                           <tr class="text-center">
                              <td colspan="4">No Records Found</td>
                           </tr>
                        <?php } ?>

                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--section-closed-->
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
</body>

</html>