<?php
session_start();
require_once(dirname(__DIR__, 2) ."/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

if (isset($_POST['add_course'])) {
   $_SESSION['msg_time'] = time();   
   if ($con->query("INSERT INTO courses SET course_name='". $_POST['course_name'] ."',description='" . ReplaceSql($_POST['description']) . "',code='". $_POST['code'] ."' ")) {
      $course_id = $con->insert_id;
   }else {
      die("MySQL Error: " . $con->error);
   }

   if (!empty($course_id)) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
      $message = "Record cannot be added";
   }
   header('Location:index.php');
}

if (isset($_POST['update_course'])) {
   $_SESSION['msg_time'] = time();
   $id = $_POST['Id'];
   $result = $con->query('UPDATE courses SET  course_name="' . $_POST['course_name'] . '",code="' . $_POST['code'] . '",description="' . $_POST['description'] . '" WHERE course_id=' . $id);
   if ($result) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record updated successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be updated';
   }
   header('Location:index.php');
}

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
   $result = $con->query("SELECT courses.* FROM courses WHERE course_id ='" . $_GET['Id'] . "'");
   if ($result) {
      if ($obj = $result->fetch_assoc()) {
         $editResult = $obj;
      }
   }
}

?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>
   <div class="form-heading">
      <div class="row">
         <div class="col-md-6">
            <h3>ADD COURSE</h3>
         </div>
         <div class="col-md-6 text-right">
            <a class="btn btn-warning" href="index.php"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; Back</a>
         </div>
      </div>
   </div>
   <form method="post">
      <div class="form-row">
         <div class="form-group col-md-6">
            <label for="course_name">Course Name</label>
            <input type="text" class="form-control" name="course_name" id="course_name" value="<?php echo (isset($editResult['course_name']) && !empty($editResult['course_name']) ? $editResult['course_name'] : '') ?>" placeholder="Course Name" required>
         </div>
         <div class="form-group col-md-6">
            <label for="code">Course Code</label>
            <input type="text" class="form-control" name="code" id="code" value="<?php echo (isset($editResult['code']) && !empty($editResult['code']) ? $editResult['code'] : '') ?>" placeholder="Code" required>
         </div>
         
      </div>
      <div class="form-group">
         <label for="description">Description</label>
         <textarea class="form-control" id="description" name="description" rows="3" required><?php echo (isset($editResult['description']) && !empty($editResult['description']) ? $editResult['description'] : '') ?></textarea>
      </div>
      <div class="form-btn">
         <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) { ?>
            <input type="hidden" value="<?php echo $editResult['course_id'] ?>" name="Id" />
            <button type="submit" name="update_course" class="btn btn-primary">Update</button>
         <?php } else { ?>
            <button type="submit" name="add_course" class="btn btn-primary">Submit</button>
         <?php } ?>
         <button type="reset" class="btn btn-danger">Cancel</button>
      </div>
   </form>
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
</body>

</html>