<?php
session_start();
require_once(dirname(__DIR__, 2) . "/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();

//Add Student Attendances------------------
if (isset($_POST['add_std_attendance'])) {
   $_SESSION['msg_time'] = time();

   $con->query("DELETE FROM class_logs WHERE class_id=" . $_POST['class_id']);
   foreach ($_POST['student_attendance'] as $student_id => $value) {

      $con->query("INSERT INTO class_logs SET class_id=" . $_POST['class_id'] . ", student_id='" . $student_id . "',class_attended =" . $value . "");
   }

   if (!empty($con->insert_id)) {
      $_SESSION['message_type'] = 'success';
      $_SESSION['message'] = 'Record Added Successfully';
   } else {
      $_SESSION['message_type'] = 'danger';
      $_SESSION['message'] = 'Record cannot be added';
      $message = "Record cannot be added";
   }
   header('Location:form.php');
}


//Session Listing------------------
$sessions = array();
$result = $con->query("SELECT * FROM sessions");
if ($result) {
   while ($obj = $result->fetch_assoc()) {
      $sessions[] = $obj;
   }
}


//Edit Student attendances---------------------
if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
   $result = $con->query("SELECT subject_classes.*,subjects.subject_name,courses.course_name FROM subject_classes INNER JOIN session_subjects ON session_subjects.session_subject_id = subject_classes.session_subject_id INNER JOIN subjects ON subjects.subject_id = session_subjects.subject_id INNER JOIN courses on courses.course_id = subjects.course_id WHERE class_id ='" . $_GET['Id'] . "'");
   if ($result) {
      if ($obj = $result->fetch_assoc()) {
         $editResult = $obj;
      }
   }
}

$courses = $con->query("SELECT * FROM courses");
?>
<?php include(DIRECTORY_PATH . '/admin/includes/head.php') ?>



<!--page-content-->
<section class="page-content">
   <?php include('../includes/alert.php'); ?>

   <div class="form-heading">
      <h3>Students Attendances</h3>
   </div>
   <div class="marks-form">
      <form method="post">
         <div class="col-md-4">
            <div class="subject-type">
               <label>Course</label>
               <select id="course_id" name="course_id" class="form-control" required>
                  <option value="">Choose course</option>
                  <?php foreach ($courses as $course) { ?>
                     <option value="<?php echo $course['course_id'] ?>" <?php echo !empty($editResult['course_id']) && $editResult['course_id'] == $course['course_id'] ? 'selected' : '' ?>><?php echo $course['course_name'] ?></option>
                  <?php } ?>
               </select>
            </div>
         </div>
         <div class="col-md-8">
            <div class="subject-type">
               <label>Subject</label>
               <select id="class_id" name="class_id" class="form-control" required>
                  <option value="">Choose Subject</option>
               </select>
            </div>
         </div>
         <div class="student-marks">
            <table class="table">
               <thead>
                  <tr>
                     <th class="border-top-0">Serial No</th>
                     <th class="border-top-0">Student Name</th>
                     <th class="border-top-0">Attendance</th>
                  </tr>
               </thead>
               <tbody class="table-head">
                  <tr class="text-center">
                     <td colspan="3"> No Records Found</td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div class="form-btn text-center buttons">
            <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) { ?>
               <input type="hidden" value="<?php echo $editResult['class_id'] ?>" name="Id" />
               <button type="submit" name="update_std_attendances" class="btn btn-primary">Update</button>
            <?php } else { ?>
               <button type="submit" name="add_std_attendance" class="btn btn-primary">Submit</button>
            <?php } ?>
            <button type="reset" class="btn btn-danger">Cancel</button>
         </div>
      </form>
   </div>
</section>
<!--page-content-->
<?php include(DIRECTORY_PATH . '/admin/includes/foot.php') ?>
<script>
   $(document).ready(function() {
      if ($('#course_id').val() == "") {
         $('#class_id').attr('disabled', true);
         $('.buttons').attr('hidden', true);
      } else {
         var course_id = $('#course_id').val();
         getSubjectClasses(course_id);
      }
      $('#session')
   });
   $(document).on('change', '#course_id', function() {

      if ($(this).val() != "") {
         $('#class_id').attr('disabled', false);
         getSubjectClasses($(this).val());
      } else {
         $('#class_id').val('');
         $('#class_id').attr('disabled', true);
      }
   });

   $(document).on('change', '#class_id', function() {
      var course_id = $("#course_id").val();
      var class_id = $(this).val();
      if (course_id == "") {
         alert("Please choose course");
         return false;
      }
      getClassStudents(course_id, class_id);

   });

   function getSubjectClasses(course_id) {
      var seleted_id = "";
      var options = "";

      var user_id = "";

      if ('<?php echo $_SESSION['user']['role_id']  ?>' == '<?php echo STAFF_ROLE_ID ?>') {
         user_id = '<?php echo $_SESSION['user']['user_id'] ?>'
      }
      $.ajax({
         url: "getSubjectClasses.php",
         type: "Get",
         data: {
            course_id: course_id,
            user_id: user_id
         },
         success: function(response) {
            $("#class_id").html('');
            $("<option></option>", {
               value: "",
               text: "Choose Subjects"
            }).appendTo('#class_id');
            $.map(JSON.parse(response), function(val, i) {
               $("<option></option>", {
                  value: val.class_id,
                  class: "text-wrap",
                  text: val.subject_name + " | " + moment(val.start_time).format('DD-MM-YYYY') + " | " + moment(val.start_time).format('hh:mm') + " - " + moment(val.end_time).format('hh:mm')
               }).appendTo('#class_id');
            });
            if ("<?php echo isset($editResult['class_id']) ? $editResult['class_id'] : "" ?>" != "") {
               selected_id = "<?php echo isset($editResult['class_id']) ? $editResult['class_id'] : "" ?>";
               $('select[id="class_id"] option[value="' + selected_id + '"]').attr("selected", "selected");
            }
         }
      });
   }

   function getClassStudents(course_id, class_id) {
      var seleted_id = "";

      var tr = "";
      $.ajax({
         url: "getClassStudents.php",
         type: "Get",
         data: {
            course_id: course_id,
            class_id: class_id
         },
         success: function(response) {
            $inc = 1;
            $('.table-head').html('');

            $('.buttons').attr('hidden', false);
            var records = JSON.parse(response);
            $.map(records['students'], function(val, i) {
               var selected_yes = "";
               var selected_no = "";
               if (records['attendances'].length > 0) {
                  selected_yes = records['attendances'][i]['student_id'] == val.student_id && records['attendances'][i]['class_attended'] == 1 ? 'checked' : '';
                  selected_no = records['attendances'][i]['student_id'] == val.student_id && records['attendances'][i]['class_attended'] == 0 ? 'checked' : '';
               }
               tr += ' <tr><td>' +
                  '<div class="display-flex">' +
                  '<h4>' + ($inc++) + '</h4>' +
                  '</div>' +
                  '</td>' +
                  '<td>' + val.first_name + ' ' + val.last_name + '</td>' +
                  '<td class="marks-div">' +
                  '<input type="radio" name="student_attendance[' + val.student_id + ']" value="1" placeholder="" ' + selected_yes + ' required><label>Yes</label>' +
                  '<input type="radio" name="student_attendance[' + val.student_id + ']" value="0" placeholder="" ' + selected_no + '><label>No</label>' +
                  '</td>' +
                  '</tr>';
            });
            $('.table-head').append(tr);
         }
      });
   }
</script>
</body>

</html>