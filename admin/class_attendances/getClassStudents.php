<?php
session_start();
require_once(dirname(__DIR__, 2) . "/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();
$result = $con->query("SELECT subject_classes.*,students.* FROM subject_classes INNER JOIN students ON students.session_id = subject_classes.session_id AND students.course_id = " . $_GET['course_id'] . " WHERE subject_classes.class_id=" . $_GET['class_id']);
$attendances = $con->query("SELECT * FROM class_logs WHERE class_id=" . $_GET['class_id']);
$records = array();
$attendance_records = array();
if ($result) {
    while ($obj = $result->fetch_assoc()) {
        $records[] = $obj;
    }
}
if ($attendances) {
    while ($obj = $attendances->fetch_assoc()) {
        $attendance_records[] = $obj;
    }
}
echo json_encode(['students' => $records, 'attendances' => $attendance_records]);
