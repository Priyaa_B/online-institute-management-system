<?php 
session_start();
require_once(dirname(__DIR__, 2) ."/database\conn.php");
$con = new DBConnection();
$con = $con->getdbconnect();
$and = '';
if(isset($_GET['user_id']) && !empty($_GET['user_id'])){
   $staff= $con->query("SELECT * FROM staffs WHERE user_id=".$_GET['user_id']."");

   if($staff){
       if($object = $staff->fetch_assoc()){
           $staff_detail = $object;
       }
   }

    $and .= ' AND session_subjects.tutored_by='.$staff_detail['staff_id'].' ';
}
$result = $con->query("SELECT subject_classes.*,subjects.subject_name FROM subject_classes INNER JOIN session_subjects ON session_subjects.session_subject_id=subject_classes.session_subject_id {$and} INNER JOIN subjects ON subjects.subject_id=session_subjects.subject_id WHERE subject_classes.course_id=".$_GET['course_id']);
$records = array();
if ($result) {
    while ($obj = $result->fetch_assoc()) {
        $records[] = $obj;
    }
}
echo json_encode($records);